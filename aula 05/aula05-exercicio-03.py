# 3) Escreva um programa em python que conte as vogais que a música ‘Faroeste Caboclo’
# tem em sua letra. Armazena a letra da música em um arquivo do tipo txt.
# Dica: Não se esqueça de considerar as letras maiúsculas, minúsculas e com acentuação.

cont_vogal = 0

with open("faroeste.txt","r") as letra:
    linhas = letra.readlines()

for n_linha in range(0,len(linhas)):
    linha = linhas[n_linha]
#    print(f"Vogais (ate o momento):   {cont_vogal}")
#    print(f"'{linha}'")
    for letra in linha:
        if letra == "A" or letra == "Á" or letra == "À" or letra == "Â" or letra == "Ã" \
        or letra == "E" or letra == "É" or letra == "Ê" or letra == "I" or letra == "Í" \
        or letra == "Î" or letra == "O" or letra == "Ó" or letra == "Ô" or letra == "Õ" \
        or letra == "U" or letra == "Ú" or letra == "a" or letra == "á" or letra == "à" \
        or letra == "â" or letra == "ã" or letra == "e" or letra == "é" or letra == "ê" \
        or letra == "i" or letra == "í" or letra == "î" or letra == "o" or letra == "ó" \
        or letra == "ô" or letra == "õ" or letra == "u" or letra == "ú":
            cont_vogal += 1
    

print(f"\nTotal de vogais na letra toda:  {cont_vogal}\n")
