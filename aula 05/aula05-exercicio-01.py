# 1) Escreva um programa em Python que simule uma dança das cadeiras. Você deverá
# importar o modulo random e iniciar uma lista com nomes de pessoas que participariam da
# brincadeira. O jogo deverá iniciar com 9 cadeiras e 10 participantes. A cada rodada,
# uma cadeira deverá ser retirada e um dos jogadores, de forma aleatória, ser eliminado. O
# jogo deverá terminar quando apenas restar uma cadeira e ao final de todas as rodadas,
# deverá ser apresentado vencedor.

# Dica: [OPCIONAL] Você poderá utilizar o modulo "time" para simular um tempo de a cada rodada para criar
# um efeito mais interessante.

# Dica: [OPCIONAL] Tentem fazer a remoção de uma pessoa aleatória por rodada sem utilizar a função "choice".

import random, time

lista = ["Joao", "Maria", "Tiago", "Amanda", "Emanuele", "Caio", "Suzana", "Miguel", 
"Rosangela", "Rian"]

while len(lista)>1:
    tamlista = len(lista)
    drop = random.randrange(0,tamlista)
    print(f"Quem saiu foi: {lista[drop]}")
    lista.pop(drop)
    espera = random.randrange(1,4)
    time.sleep(espera)

print(f"Quem venceu foi: {lista[0]}")    