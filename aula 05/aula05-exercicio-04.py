# 4) Escreva um programa em python que realize um cadastro. Deverão ser coletadas as
# seguintes informações:

# CPF
# Nome
# Idade
# Sexo
# Endereço

# Os registros deverão ser armazenados em um arquivo CSV. Caso desejar manter o padrão
# brasileiro, o CSV será separado pelo caractere ;

import os, csv

if os.name == "nt":
    osclear = "cls"
else:
    osclear = "clear"

cabecalho = ""

with open("aula 05/cadastro.csv","r") as teste:
    conteudo = csv.reader(teste, delimiter=";")  # generator - a variavel conteudo se torna
    for linha in conteudo:
        print(linha)
        if linha == ["CPF","Nome","Idade","Sexo","Endereco"]:
            cabecalho = "Y"
            break
        else:
            cabecalho = "N"
            break
    if cabecalho == "":
        cabecalho = "N"

while True:
    os.system(osclear)
    print("""
**************** Cadastro de Pessoas ***************
      Digite os dados pessoais solicitados 
  (ou digite 0 no campo CPF para sair do programa)
 """)
    nCPF =  input("  CPF:       ")
    if nCPF == "0":
        print("\n Encerrando o programa...")
        break

    nome =  input("  Nome:      ")
    idade = input("  Idade:     ")
    sexo =  input("  Sexo:      ")
    ender = input("  Endereco:  ")

    if cabecalho == "N":
        with open("aula 05/cadastro.csv","w") as arquivo:
            linha = csv.writer(arquivo, delimiter=";")
            minha_linha = ["CPF","Nome","Idade","Sexo","Endereco"]
            linha.writerow(minha_linha)
        cabecalho = "Y"

    with open("aula 05/cadastro.csv","a",newline="") as arquivo:
        linha = csv.writer(arquivo, delimiter=";")
        minha_linha = [nCPF,nome,idade,sexo,ender]
        linha.writerow(minha_linha)





