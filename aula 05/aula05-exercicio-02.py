# 2) Crie um programa que pergunte para o usuario um numero de pessoas a participarem de um sorteio (2-20),
#  e o numero de pessoas a serem sorteadas, e depois sorteie esse numero de pessoas da lista.

# O programa deverá pegar o numero de pessoas a participar aleatoriamente desta lista:

import random

lista = ["Joao", "Maria", "Tiago", "Amanda", "Emanuele", "Caio", "Suzana", "Miguel", 
"Rosangela", "Rian", "Lucimar", "Ulisses", "Leonardo", "Kaique", "Bruno", "Raquel", 
"Benedito", "Tereza", "Valmir", "Joaquim"]

# Nota: A mesma pessoa não pode ganhar duas vezes.

n_particip = int(input("\nInforme o numero de participantes nos sorteios (de 2 a 20):  "))
while True:
    n_sorteios = int(input(f"Informe o numero de sorteios (de 1 a {n_particip}):  "))
    if n_particip >= n_sorteios:
        break
    print(f"\nNumero de sorteios nao pode ser maior que o numero de participantes!\n")

particip = []

for part in range(0,n_particip):
    tamlista = len(lista)
    n_pessoa = random.randrange(0,tamlista)
    particip.append(lista[n_pessoa])
    print(f"O {part+1}.o participante e:  {lista[n_pessoa]}")
    lista.pop(n_pessoa)

print()  

for sorteado in range(0,n_sorteios):
    tamlista = len(particip)
    ganhou = random.randrange(0,tamlista)
    print(f"Quem ganhou o {sorteado+1}.o sorteio foi: {particip[ganhou]}")
    particip.pop(ganhou)

print()    
