# Persistencia de dados com OPEN

# Modos:
#  w - write
#  r - read
#  a - append

# nome = input("Qual o seu nome ?  ")

# # Abrindo o arquivo para gravacao
# arquivo = open("nomes.txt","a")
# arquivo.write(f"{nome}\n")
# arquivo.close()

# # Abrindo o arquivo para leitura
# arquivo = open("nomes.txt","r")
# conteudo = arquivo.read()
# print(f"Segue o conteudo do arquivo:\n**************************\n{conteudo}\n**************************")
# arquivo.close()

# # Contendo o uso do aruivo em um bloco (nao precisa solicitar o close do arquivo)
# with open("nomes.txt","a") as arquivo:
#     arquivo.write("Testando o 'with open'")

# ==========================================================
import csv

with open("planilha.csv","r") as arquivo:
    conteudo = csv.reader(arquivo, delimiter=";")  # generator - a variavel conteudo se torna
    # print(conteudo)   # nao funciona desse modo
    
    for linha in conteudo:
        print(linha)
