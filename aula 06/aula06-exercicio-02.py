# 2) Implemente um programa que represente uma fila. O contexto do programa é uma
# agência de banco. Cada cliente ao chegar deverá respeitar a seguinte regra: o primeiro
# a chegar deverá ser o primeiro a sair. Você poderá representar pessoas na fila a par-
# tir de números os quais representam a idade. A sua fila deverá conter os seguintes
# comportamentos:

# • Adicionar pessoa na fila: adicionar uma pessoa na fila.
# • Atender Fila: atender a pessoa respeitando a ordem de chegada
# • Dar prioridade: colocar uma pessoa maior de 65 anos como o primeiro da fila

class Fila:

    def __init__(self):
        self.pessoas = 0
        self.prioridades = 0
        self.ordem = []

    def em_espera(self, idade):
        idade = int(idade)
        if idade > 64:
            lugar = self.prioridades
            self.prioridades += 1
            self.ordem.insert(lugar,idade)
            return(f"{idade} esta na fila prioritaria!")
        else:
            self.ordem.append(idade)
            return(f"{idade} esta na fila.")
    
    def em_atendimento(self):
        if len(self.ordem) < 1:
            return("Nao ha ninguem na fila esperando atendimento.")
        else:
            idade = self.ordem[0]
            self.ordem.pop(0)
            if self.prioridades > 0:
                self.prioridades -=1
            return(f"\n{idade} sera atendido agora.\n")
        
    def lista_espera(self):
        return(f"\nOrdem da lista de espera da fila: \n{self.ordem}\n")
        
fila = Fila()

print(fila.em_espera(40))
print(fila.em_espera(25))
print(fila.lista_espera())
print(fila.em_espera(70))
print(fila.em_espera(32))
print(fila.em_espera(22))
print(fila.em_espera(87))
print(fila.lista_espera())
print(fila.em_atendimento())
print(fila.lista_espera())

