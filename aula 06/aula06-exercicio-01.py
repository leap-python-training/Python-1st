# 1) Crie uma classe que represente um ônibus. O ônibus deverá conter os seguintes atributos:

# capacidade total
# capacidade atual
# movimento

# Os comportamentos esperados para um Ônibus são:
# Embarcar
# Desembarcar
# Acelerar
# Frear

# Lembre-se que a capacidade total do ônibus é de 45 pessoas - não será possível admitir super-
# lotação. Além disso, quando o ônibus ficar vazio, não será permitido efetuar o desembarque
# de pessoas. Além disso, pessoas não podem embarcar ou desembarcar com o onibus em movimento.

class Onibus:

    def __init__(self):
        self.lotacao_maxima = 45
        self.lotacao_atual = 0
        self.em_movimento = False

    def embarcar(self, passageiros):
        passageiros = int(passageiros)
        print(f"Em movimento: {self.em_movimento}")
        if self.em_movimento == True:
            return(f"Impossivel embarcar passageiros, o onibus esta em movimento!")
        elif self.lotacao_atual == self.lotacao_maxima:
            return("Impossivel embarcar mais passageiros, o onibus ja esta na lotacao maxima!")
        elif self.lotacao_atual + passageiros > self.lotacao_maxima:
            lugares = self.lotacao_maxima - self.lotacao_atual
            return(f"Impossivel embarcar todos os novos passageiros, so e possivel aceitar mais {lugares} passageiros!")
        else:
            self.lotacao_atual += passageiros
            return(f"{passageiros} passageiros embarcados.")
                
    def desembarcar(self, passageiros):
        passageiros = int(passageiros)
        print(f"Em movimento: {self.em_movimento}")
        if self.em_movimento == True:
            return(f"Impossivel desembarcar passageiros, o onibus esta em movimento!")
        elif self.lotacao_atual == 0:
            return(f"Impossivel desembarcar passageiros, o onibus esta vazio!")
        elif self.lotacao_atual - passageiros < 0:
            return(f"Impossivel desembarcar tantos passageiros, o onibus so tem {self.lotacao_atual} passageiros!")
        else:
            self.lotacao_atual -= passageiros
            return(f"{passageiros} passageiros desembarcados.")
        
    def acelerar(self):
        if self.em_movimento == True:
            return("O onibus ja esta em movimento!")
        else:
            self.em_movimento = True
            print(f"Em movimento: {self.em_movimento}")
            return("O onibus comecou a mover.")

    def frear(self):
        if self.em_movimento == False:
            return("O onibus ja esta parado.")
        else:
            self.em_movimento = False
            print(f"Em movimento: {self.em_movimento}")
            return("O onibus parou.")

meu_onibus = Onibus()

print(meu_onibus.embarcar(8))
print(meu_onibus.acelerar())
print(meu_onibus.desembarcar(3))
print(meu_onibus.frear())
print(meu_onibus.desembarcar(2))
print(meu_onibus.embarcar(29))
print(meu_onibus.acelerar())
print(meu_onibus.embarcar(4))
print(meu_onibus.frear())
print(meu_onibus.embarcar(8))
print(meu_onibus.embarcar(6))
print(meu_onibus.embarcar(2))
print(meu_onibus.acelerar())
print(meu_onibus.frear())
print(meu_onibus.desembarcar(27))
print(meu_onibus.desembarcar(20))
print(meu_onibus.desembarcar(18))
print(meu_onibus.desembarcar(5))

