# Classes  -> Tipos de objetos

# Vamos criar uma classe que represente uma pilha

class Pilha:

    def __init__(self):
        self.pilha = list()
        self.limite = 5
        self.quantidade = 0

    def empilhar(self, item):
        if self.quantidade < self.limite:
            self.pilha.append(item)
            self.quantidade += 1
            return(f"{item} adicionado a pilha")
        else:
            return(f"Essa pilha ja possui {self.limite} itens!")
        
    def desempilhar(self):
        if self.quantidade > 0:
            item = self.pilha[-1]
            self.pilha.pop(-1)
            self.quantidade -= 1
            return(f"Item {item} removido da pilha")
        else:
            return(f"A pilha ja esta vazia!")

pilha_de_pratos = Pilha()
pilha_de_roupas = Pilha()

# print(pilha_de_pratos.limite)
# print(pilha_de_pratos.quantidade)

# print(pilha_de_pratos.empilhar("Prato de vidro"))
# print(pilha_de_pratos.empilhar("Prato de porcelana"))
# print(pilha_de_pratos.empilhar("Prato de ceramica"))
# print(pilha_de_pratos.empilhar("Prato de madeira"))
# print(pilha_de_pratos.empilhar("Prato de plastico"))
# print(pilha_de_pratos.empilhar("Prato de metal"))

# print(pilha_de_pratos.quantidade)

# print(pilha_de_pratos.desempilhar())
# print(pilha_de_pratos.desempilhar())
# print(pilha_de_pratos.desempilhar())
# print(pilha_de_pratos.desempilhar())
# print(pilha_de_pratos.desempilhar())
# print(pilha_de_pratos.desempilhar())

# print(pilha_de_pratos.quantidade)

# ====================================================================
# Encapsulamento
# Para restringir acesso externo a propriedades da classe podemos encapsular essas 
# propriedades simplesmente colocando dois caracteres underline antes do nome da propriedade

class Pilha2:

    def __init__(self):
        self.__pilha2 = list()
        self.__limite = 5
        self.__quantidade = 0

    def empilhar(self, item):
        if self.__quantidade < self.limite:
            self.__pilha2.append(item)
            self.__quantidade += 1
            return(f"{item} adicionado a pilha")
        else:
            return(f"Essa pilha ja possui {self.__limite} itens!")
        
    def desempilhar(self):
        if self.__quantidade > 0:
            item = self.__pilha2[-1]
            self.__pilha2.pop(-1)
            self.__quantidade -= 1
            return(f"Item {item} removido da pilha")
        else:
            return(f"A pilha ja esta vazia!")

# ===================================================================
# Heranca
        
class Funcionario:

    def __init__(self):
        self.salario = 0.00
        self.nome = ""

    def define_nome(self, x):
        self.nome = x

    def define_salario(self, y):
        self.salario = y

class Gerente(Funcionario):

    def __init__(self):
        self.bonus = 1.3

    def aplica_bonus(self):
        self.salario =  self.salario * self.bonus


# joao = Funcionario()
# joao.define_nome("Joao")
# joao.define_salario(3000.00)
# print(joao.salario)

# pedro = Gerente()
# pedro.define_nome("Pedro")
# pedro.define_salario(3000.00)
# pedro.aplica_bonus()
# print(pedro.salario)


# ===========================================================================
# Polimorfismo

# class Funcionario2:

#     def __init__(self):
#         self.salario = 3000.00
#         self.nome = ""

#     def define_nome(self, x):
#         self.nome = x



# class Gerente2(Funcionario2):

#     def __init__(self):
#         self.salario = 4500.00


# manoel = Funcionario2()
# manoel.define_nome("Manoel")

# tiago = Gerente2()
# tiago.define_nome("Tiago")
# print(tiago.salario)


