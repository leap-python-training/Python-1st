# Tipos Primitivos no Python

# String  ->  str   -> Frases, conjuntos de caracteres, sempre entr aspas
# Integer ->  int   -> Numeros inteiros, sem aspas
# Float   ->  float -> Numeros decimais, numeros reais, numeros com ponto, sem aspas
# Boolean ->  bool  -> True ou False, primeira letra sempre maiuscula, sem aspas

nome = "Luiz Polastre"  # String
peso = "100"            # String
idade = 46              # Integer 
altura = 1.75           # Float
loiro = False           # Boolean

#======================================================

numero1 = 15
numero2 = 25

numero3 = numero1 + numero2
print(numero3)

#======================================================

# Todo input recebido eh do tipo String
#idade = input("Qual a sua idade ?  ")

# Eh possivel converter o tipo de dado usando a abreviacao do tipo como uma funcao
# Mas se tentar converter algo incompativel (por ex. um texto para inteiro) vai dar erro
#idade = int(idade)

#======================================================
# Metodos do Python

# .Capitalize() coloca soh a primeira letra em maiuscula e o resto em minuscula
# .Title() coloca a primeira letra de cada palavra em maiuscula e o resto em minuscula
# .Upper() coloca tudo em maiuscula
# .Replace() permite trocar letras da frase por outras
# .Isallnum() retorna se o conteudo eh so alfanumerico
# .Islower() retorna se as letras do conteudo sao minusculas

nome = "luiz polastre"
print(nome)
print(nome.islower())

#==================================================================

idade = int(input("Qual a sua idade ?  "))

if idade > 17:    #if
    print("Voce pode ser preso como adulto!")
    print("Aja com responsabilidade!")

elif idade > 15:  #elif eh o short pra else if - o numero de elifs eh ilimitado
    print("Esta mais que na hora de comecar a ser responsavel!") 

else:             #else eh o que executa se todos os testes anteriores falharam
    print("Nao seja descabecado!")
print("Fim do check")


#==================================================================

# Para comparar valores: 
#   use '==' para comparar se sao iguais
#   use '!=' para comparar se sao diferentes

# Estruturas de repeticao

# FOR e WHILE
# O loop eh limitado pela indentacao, nao ha necessidade de informar o fim do loop

contador = 10

while contador !=0:
    print("Estou contando...")
    print("Executando codigo...")

    contador = contador - 1

while True:
    resposta = input("Quer parar o programa ? [s/n]: ")
    if resposta == "s":
        break

    resposta2 = input("Quer continuar a execucao atual ? [s/n]: ")
    if resposta2 == "s":
        continue    # o CONTINUE ignora o restante dos comandos do loop e retorna ao topo
                    # i.e., similar ao Iterate do REXX
    
    while True:
        print("Teste")
        break       # o BREAK sempre quebra o loop mais recente

for cont in range(0,10):
    print(f"Imprimindo mensagem: {cont}")
