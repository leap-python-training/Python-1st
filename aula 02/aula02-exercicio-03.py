# Exercicio 03 da aula 02:

print("Identifique a sua geracao:")
anonasc = int(input("Entre com o ano do seu nascimento: "))
print(" ")

# Pode testar multiplas condicoes se usar 'and' e 'or' 
if anonasc < 1965:
    print(f". ano nascimento = {anonasc}: Geracao: Baby Boomer")

elif anonasc < 1979: 
    print(f". ano nascimento = {anonasc}: Geracao: X")

elif anonasc < 1995: 
    print(f". ano nascimento = {anonasc}: Geracao: Y")

else: 
    print(f". ano nascimento = {anonasc}: Geracao: Z")



