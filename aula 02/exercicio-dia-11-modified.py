# Exercicio de entrada e apresentacao de dados

# Solicitando os dados:
print("Gestao de Cadastro ")
print(" Entre com os dados solicitados:")
Nome = input("   - Entre com o seu nome: ")
CPF = input("   - Entre com o seu CPF: (use o formato xxx.xxx.xxx-xx) ")
Idade = input("   - Entre com a sua idade: ")

# Apresentando os dados:
print(" ")
print("-------------------------------------")
print(" Confirmacao de cadastro: ")
# o .format() eh um padrao mais antigo, mais usado no Python 2
print(" Nome: {}".format(Nome))
# o  print(f"") eh um padrao mais novo e tambem formata a string
print(f" CPF: {CPF}")
print(f" Idade: {Idade}")
print("-------------------------------------")
print(" ")

# Um metodo alternativo de imprimir multiplas linhas de uma vez:

print(f""" 
-------------------------------------
 Confirmacao de cadastro: 
 Nome: {Nome}
 CPF: {CPF}
 Idade: {Idade}
-------------------------------------
 """)