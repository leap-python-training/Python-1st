# Exercicio 04 da aula 02:

saida1 = "N"
bananas = 0
melancias = 0
morangos = 0

while saida1 == "N":
    print(""" 
=======================
Quitanda:
1: Ver cesta
2: Adicionar frutas
3: Sair
=======================
 """)
    opcao1 = input("Digite a opcao desejada: ")
    print()

    if opcao1.isnumeric() == False:
        print(f"Opcao {opcao1} invalida! - selecione 1, 2 ou 3")
        print()
        continue
    else:
         opcao1 = int(opcao1)

    if opcao1 == 1:
         print(f"""Conteudo da cesta:
   {bananas} bananas
   {melancias} melancias
   {morangos} morangos
 """)
         
    elif opcao1 == 2:
        saida2 = "N"

        while saida2 =="N":
            print("""
===========================
Menu de frutas
 Digite a opcao desejada:
 Escolha a fruta desejada:
   1 - Banana
   2 - Melancia
   3 - Morango
===========================
 """)
            opcao2 = input("Digite a opcao desejada: ")

            if opcao2.isnumeric() == False:
                print(f"Opcao {opcao2} invalida! - selecione 1, 2 ou 3")
                print()
                continue
            else:
                opcao2 = int(opcao2)

            if opcao2 == 1:
                 qtdeok = False
                 while qtdeok == False:
                    qtde = input("Digite a quantidade desejada: ")
                    if qtde.isnumeric() == False:
                        print(f"{qtde} nao e valida! - digite a quantidade desejada")
                        print()
                        continue
                    else:
                        qtde = int(qtde)
                        qtdek = True

                 bananas = bananas + qtde
                 print(f" {qtde} bananas adicionadas a cesta com sucesso!")
                 print()
                 saida2 = "S"

            elif opcao2 == 2:
                 qtdeok = False
                 while qtdeok == False:
                    qtde = input("Digite a quantidade desejada: ")
                    if qtde.isnumeric() == False:
                        print(f"{qtde} nao e valida! - digite a quantidade desejada")
                        print()
                        continue
                    else:
                        qtde = int(qtde)
                        qtdek = True
                 melancias = melancias + qtde
                 print(f" {qtde} melancias adicionadas a cesta com sucesso!")
                 print()
                 saida2 = "S"

            elif opcao2 == 3:
                 qtdeok = False
                 while qtdeok == False:
                    qtde = input("Digite a quantidade desejada: ")
                    if qtde.isnumeric() == False:
                        print(f"{qtde} nao e valida! - digite a quantidade desejada")
                        print()
                        continue
                    else:
                        qtde = int(qtde)
                        qtdek = True

                 morangos = morangos + qtde
                 print(f" {qtde} morangos adicionados a cesta com sucesso!")
                 print()
                 saida2 = "S"
                 
            else:
                 print(f"Opcao {opcao2} invalida! - selecione 1, 2 ou 3")
                 print()

    elif opcao1 == 3:
         saida1 = "S" 

    else:
        print(f"Opcao {opcao1} invalida! - selecione 1, 2 ou 3")
        print()      


