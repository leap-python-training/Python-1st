# Exercicio 2:
# Escreva uma calculadora utilizando funções
# Ela pergunta dois numeros, e da as opções de calculo.
# (soma, diferença, multiplicação, divisão)

import os, time

def soma(x,y):
    return x+y

def subtr(x,y):
    return x-y

def multi(x,y):
    return x*y

def div(x,y):
    return x/y

while True:
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")

    print("=========== Calculadora =========")
    print("  Selecione a operacao desejada: ")
    print("     ⇒ 1 - Soma ")
    print("     ⇒ 2 - Subtracao ")
    print("     ⇒ 3 - Multiplicacao ")
    print("     ⇒ 4 - Divisao ")
    print("     ⇒ S - Sair ")
    print()
    opcao = input("      Digite a opcao desejada:  ")

    if opcao == "1":
        print()
        valor1 = int(input("   Digite o valor da 1.a parcela: "))
        valor2 = int(input("   Digite o valor da 2.a parcela: "))
        print(f"   {valor1}  +  {valor2}  =  {soma(valor1,valor2)}")
        print()
        input(" Aperte ENTER para continuar... ")

    elif opcao == "2":
        print()
        valor1 = int(input("   Digite o valor do minuendo: "))
        valor2 = int(input("   Digite o valor do subtraendo: "))
        print(f"   {valor1}  -  {valor2}  =  {subtr(valor1,valor2)}")
        print()
        input(" Aperte ENTER para continuar... ")

    elif opcao == "3":
        print()
        valor1 = int(input("   Digite o valor do 1.o fator: "))
        valor2 = int(input("   Digite o valor do 2.o fator: "))
        print(f"   {valor1}  ×  {valor2}  =  {multi(valor1,valor2)}")
        print()
        input(" Aperte ENTER para continuar... ")

    elif opcao == "4":
        print()
        valor1 = int(input("   Digite o valor do dividendo: "))
        valor2 = int(input("   Digite o valor do divisor: "))
        print(f"   {valor1}  ÷  {valor2}  =  {div(valor1,valor2)}")
        print()
        input(" Aperte ENTER para continuar... ")

    elif opcao == "S" or opcao == "s":
        if os.name == "nt":
            os.system("cls")
        else:
            os.system("clear")
        print("Obrigado por usar a calculadora")
        break

    else:
        print()
        print("Opcao invalida! Selecione entre as opcoes acima!")
        time.sleep(2)

