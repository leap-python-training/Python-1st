# Modulos *************************************************************************
# Modulos expandem as funcionalidades do Python

# 1/3 -> Importando um modulo nativo do Python
# import os, math, sqlite3, time
import os, time

if os.name == "nt":
    osclear = "cls"
else:
    osclear = "clear"

# time.sleep(2)

# 1/3(.1) -> Importando uma funcao apenas de um modulo
# from math import ceil
# print(ceil(5.8))


# 2/3 -> Instalando modulos com PIP
# - em linux (no terminal): pip3 install flask  
#  -- quando instalar pode ser necessario rodar o comando usando SUDO ou um superusuario
# - em windows (na console): pip install flask
# apos instalar pode importar normalmente 
import flask


# 3/3 -> Instalando modulos de terceiros
# import samplemod
# print(samplemod.nome)

# 3/3(.1) -> Importando apenas parte do modulos de terceiros
# from samplemod import nome
# print(nome)



# Funcoes *************************************************************************
# Funcoes sao trechos de codigo que podem ser definidos para serem chamados e 
#  reutilizados no decorrer do programa

def limpa_tela():
    os.system(osclear)

# def saudacao(x):
#     print(f"Bem vindo ao Python {x}")

# nome1 = input("Qual e o seu nome: ")

limpa_tela()

# saudacao(nome1)

# ==========================

# def soma(x, y):
#     z = x + y
#     print(z)

# soma(18,23)

# ==========================

# def maior(c):
#     if c >=18:
#         return True
#     else:
#         return False
    
# idade =int(input("Qual a sua idade ?  "))
# resposta = maior(idade)

# if maior(idade) == True:
#     print("Pode entrar.")

# ==========================

# lista =[10, 22, 67]

# def soma(*x):
#     valor = 0
#     for numero in x:
#         valor += numero
#     lista.append(valor)
#     return valor

# print(soma(4, 12, 55, 63))
# print(soma(7, 81, 5.75))
# print(lista)

# ==========================

# def subtracao(x, y):
#     z = x - y
#     return z

# print(subtracao(10,5))  # nesse caso, o 1.o valor eh assumido como x e o 2.o como y
# print(subtracao(y=20,x=8))  # ou podemos explicitar qual valor usar para cada variavel

# ==========================

# def multiplicacao(*, n1, n2):  # quando colocamos o * antes dos parametros exigimos
#                                #  que as variaveis sejam explicitadas    
#     return n1 * n2

# print(multiplicacao(n1=4, n2=3))

# ==========================

# def divisao(x, y, /):  # quando colocamos a / apos os parametros o python nao permite 
#                        # que especifiquemos qual valor corresponde a qual variavel 
#                        # (basicamente o oposto de colocar * antes dos parametros)
#     return x / y

# print(divisao(8,2))

# ==========================

# def checa_idade(**parametro): # quando eh passado com ** antes da variavel, o que for 
#                               # passado sera tratado como dicionarios dentro da funcao
#     for pessoa, idade in parametro.items():
#         if idade > 17:
#             print(f" {pessoa} maior de idade")
#         else:
#             print(f" {pessoa} menor de idade")

# checa_idade(a=22, b=14, c=30, d=17, e=20)

# ==========================
# Funcoes lambda / funcoes anonimas

def soma(x,y):   # essa eh uma funcao normal
    return x+y

print(soma(2,5))

soma2 = lambda x, y: x + y  # essa eh uma funcao lambda

print(soma2(2,5))













