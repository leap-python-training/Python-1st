# Exercicio 1:
# Escreva uma função que receba um nome e que tenha como saída uma saudação.
# O argumento da função deverá ser o nome.

import os

def saudacao(x):
    print(f"Ola {x}, bem-vindo ao Python!")
    print()
    

if os.name == "nt":
    os.system("cls")
else:
    os.system("clear")

nome = input("Qual o seu nome ?  ")
saudacao(nome)

