# Assumindo que uma lata de tinta pinta 3m², escreva um programa
# que possua uma função que receba as dimenções de uma parede,
# passadas pelo usuario, calcule sua área, e mostre uma mensagem
# dizendo quantas latas de tinta seriam necessárias para pintar
# essa parede.

import os
from math import ceil

def numero_latas(x,y):
    latas=ceil(x*y/3)
    return latas
    

if os.name == "nt":
    os.system("cls")
else:
    os.system("clear")

print("=== Calculo da quantidade de latas de tinta ===")
altura = float(input("Informe a altura da parede:   "))
largura = float(input("Informe a largura da parede:  "))
print()
print(f" Para pintar uma parede de {altura:.2f}m de altura e {largura:.2f}m de largura")
print(f"  serao necessarias {numero_latas(altura,largura)} latas de tinta.")
print()