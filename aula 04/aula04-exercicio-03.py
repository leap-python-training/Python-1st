# Exercicio 3:
# Escreva um programa que possua uma função que conte o
# numero de números pares passados à ela, pelo usuário.

import os

def contapar(argumentos):
    print(argumentos)
    valor_pares = 0
    for valor in argumentos:
        if valor % 2 == 0:
            valor_pares += 1
    return valor_pares

lista = []

if os.name == "nt":
    os.system("cls")
else:
    os.system("clear")

print("=== Entre com uma sequencia de valores inteiros ===")
print("  (digite um caracter nao-numerico para finalizar)")
contador=0
while True:
    contador += 1
    strvalor = input(f" - digite o {contador}.o valor:  ")
    if strvalor.isnumeric() == False or strvalor == "0":
        contador -=1
        if contador == 0:
            print()
            print("Nenhum valor foi entrado; terminando o programa....")
            break
        else:
            print(lista)
            pares = contapar(lista)
            print()
            print(f" Voce passou {pares} numeros pares!")
            break
    else:
        valor = int(strvalor)
        lista.append(valor)
    

