# Generators:

# generators podem ser usados para criar dados

# def cria_lista(s):
#     lista = []
#     for numero in range(0,s):
#         lista.append(numero)
#     return lista

# def cria_generator(s):
#     for numero in range(0,s):
#         yield numero

# # print(cria_lista(15))

# gen = cria_generator(15)

# print (next(gen))
# print (next(gen))
# print (next(gen))
# print (next(gen))

# for x in gen:
#     print(x)

# print (next(gen))

# def gen():
#     for x in range(0,10):
#         yield x

# the generator above is equivalent to the generator below:
# Generator Comprehensions:

# gencomp = (x for x in range(0,10))

# for z in range(0,5):
#     print(next(gencomp))


# the comprehension can be made to a list too
    
# def lista_dobro(x):
#     lista = []
#     for numero in range(0,x):
#         lista.append(numero * 2)
#     return lista

# lista_1 = lista_dobro(10)
# print(lista_1)
# # isso pode ser substituido por:
# lista_dobre =[x *2 for x in range(0,10)]
# print(lista_dobre)


# # Dict comprehension

# # this is a regular generator
# def gera_dict():
#     dicionario = {}
#     for chave, valor in enumerate(range(550,560)):
#         dicionario[chave] = valor
#     return dicionario

# dici = gera_dict()
# print(dici)

# # and this is the equivalent comprehensive one
# dict_comp = {chave: valor for chave, valor in enumerate(range(550,560))}
# print(dict_comp)

# ============================================================================
# map

# def quadrado(x):
#     return x**2

# quadrado2 = lambda x: x**2

# lista = [1, 2, 3, 4, 5]

# resultado1 = map(quadrado,lista)
# print(list(resultado1))

# resultado2 = map(quadrado2,lista)
# print(list(resultado2))


# reduce
from functools import reduce

lista = [5, 10, 15, 20, 25]

def soma(x,y):
    return x + y

soma2 = lambda x, y: x + y

print(reduce(soma, lista))

#filter 
temperaturas = [25, 30, 35, 36, 12, 21, 41, 22, 23, 36]

sub30 = lambda x: x <30

print(list(filter(sub30,temperaturas)))
