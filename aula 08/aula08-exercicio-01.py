# ====================================================================================
# 1) Escreva um programa utilizando funções que realize um cadastro.
# Deverão ser coletadas as seguintes informações:

# CPF
# Nome
# Idade
# Sexo
# Cidade

# Os registros deverão ser armazenados em um arquivo CSV.
# Para manter o padrão brasileiro, o CSV será separado pelo caractere ";".
# O programa deverá possuir uma função de consulta e de exclusão (POR NOME OU CPF).
# O programa deverá possuir tratamentos de erro, com a finalidade de que o programa nunca
# dê uma exceção e também que ele não aceite dados incorretos em nenhum momento.

# 1: Importar os modulos a serem usados.
# 2: Criamos uma função para o menu principal do programa.
# 3: Criar uma função para coletar as informações do cliente.
# 4: Criar uma função para validar as informações passadas pelo cliente.
# 5: Criar uma função para adicionar essas informações em um arquivo CSV.
# 6: Criar uma função para consultar o arquivo CSV.
# 7: Criar uma função para remover entradas do arquivo CSV.


import os, csv
from time import sleep


if os.name == "nt":
    osclear = "cls"
else:
    osclear = "clear"

cabecalho = ""

with open("aula 08/cadastro.csv","a+") as teste:
    conteudo = csv.reader(teste, delimiter=";")  # generator - a variavel conteudo se torna
    for linha in conteudo:
        print(linha)
        if linha == ["CPF","Nome","Idade","Sexo","Cidade"]:
            cabecalho = "Y"
            break
        else:
            cabecalho = "N"
            break
    if cabecalho == "":
        cabecalho = "N"



def checa_CPF(cpf_info):
    if cpf_info.isnumeric() == False:
        return 1
    elif cpf_info == "":
        return 2
    else:
        return 0


def checa_nome(nome_info):
    if nome_info == "":
        return 1
    else:
        return 0


def cadastra():
    global cabecalho
    while True:
        os.system(osclear)
        print("""
**************** Cadastro de Novas Pessoas ***************
            Digite os dados pessoais solicitados 
            (ou digite 0 no campo CPF para sair)
    """)
        nCPF =  input("    CPF:       ")
        cpf_check = checa_CPF(nCPF)
        if cpf_check == 1:
            print(f"\n\n\n\n\n  <<< O valor {nCPF} nao e valido como CPF! >>> \n")
            sleep(3)
            continue
        elif cpf_check == 2:
            print(f"\n\n\n\n\n  <<< O CPF deve ser um valor numerico! >>> \n")
            sleep(3)
            continue
        if nCPF == "0":
            print("\n\n\n\n\n  >>> Encerrando o cadastro de novas pessoas ...")
            break
        while True:
            nome =  input("    Nome:      ")
            nome_check = checa_nome(nome)
            if nome_check == 1:
                print(f"\n\n\n\n  <<< Digite o nome da pessoa! >>> \n")
                sleep(3)
                os.system(osclear)
                print(f"""
**************** Cadastro de Novas Pessoas ***************
            Digite os dados pessoais solicitados 
            (ou digite 0 no campo CPF para sair)
                  
    CPF:       {nCPF}""")
                continue
            else:
                break

        while True:
            try: 
                idade = int(input("    Idade:     "))
            except ValueError:
                print(f"\n\n\n  <<< Digite a idade da pessoa! >>> \n")
                sleep(3)
                os.system(osclear)
                print(f"""
**************** Cadastro de Novas Pessoas ***************
            Digite os dados pessoais solicitados 
            (ou digite 0 no campo CPF para sair)
                  
    CPF:       {nCPF}
    Nome:      {nome}""")
                continue
            if idade < 1:
                print(f"\n\n\n  <<< Digite a idade da pessoa! >>> \n")
                sleep(3)
                os.system(osclear)
                print(f"""
**************** Cadastro de Novas Pessoas ***************
            Digite os dados pessoais solicitados 
            (ou digite 0 no campo CPF para sair)
                  
    CPF:       {nCPF}
    Nome:      {nome}""")
                continue
            else:
                break
        while True:
            sexo =  input("    Sexo:      ")
            if sexo == "":
                print(f"\n\n  <<< Campo sexo nao pode ficar vazio! >>> \n")
                sleep(3)
                os.system(osclear)
                print(f"""
**************** Cadastro de Novas Pessoas ***************
            Digite os dados pessoais solicitados 
            (ou digite 0 no campo CPF para sair)
                  
    CPF:       {nCPF}
    Nome:      {nome}
    Idade:     {idade}
    Sexo:      {sexo}""")
                continue
            else:
                break
        while True:
            cidad = input("    Cidade:    ")
            if cidad == "":
                print(f"\n  <<< Campo Cidade nao pode ficar vazio! >>> \n")
                sleep(3)
                os.system(osclear)
                print(f"""
**************** Cadastro de Novas Pessoas ***************
            Digite os dados pessoais solicitados 
            (ou digite 0 no campo CPF para sair)
                  
    CPF:       {nCPF}
    Nome:      {nome}
    Idade:     {idade}""")
                continue
            else:
                break
        
        confirma_cadastro = input("\n    Confirma o novo cadastro com os dados acima ? (S confirma, senao cancela)  ")
        if confirma_cadastro.upper() != "S":
            continue

        if cabecalho == "N":
            with open("aula 08/cadastro.csv","w") as arquivo:
                linha = csv.writer(arquivo, delimiter=";")
                minha_linha = ["CPF","Nome","Idade","Sexo","Cidade"]
                linha.writerow(minha_linha)
            cabecalho = "Y"

        with open("aula 08/cadastro.csv","a",newline="") as arquivo:
            linha = csv.writer(arquivo, delimiter=";")
            minha_linha = [nCPF,nome,idade,sexo,cidad]
            linha.writerow(minha_linha)
        
        print(f"  >>> {nome}, CPF {nCPF} cadastrado com sucesso!")
        sleep(2)



def consulta():
    nome = ""
    nCPF = ""
    while True:
        os.system(osclear)
        print("""
************* Consulta de Pessoas Cadastradas ************
                Informe os dados solicitados 
\n    """)
        opcao = input("""    Selecione a informacao para buscar: 
      - N para buscar pelo nome
      - C para buscar pelo CPF 
      - F para finalizar consulta
                      
    Informe a opcao desejada:  """)
        if opcao.upper() == "C":
            while True:
                nCPF =  input("\n    Informe o CPF a ser buscado:    ")
                cpf_check = checa_CPF(nCPF)
                if cpf_check == 1:
                    print(f"\n  <<< O valor {nCPF} nao e valido como CPF! >>> \n")
                    sleep(3)
                    os.system(osclear)
                    print(f"""
************* Consulta de Pessoas Cadastradas ************
                Informe os dados solicitados 
\n\n    
    Selecione a informacao para buscar: 
      - N para buscar pelo nome
      - C para buscar pelo CPF 
      - F para finalizar consulta
                      
    Informe a opcao desejada:  {opcao}""")
                    continue
                elif cpf_check == 2 or nCPF == "0":
                    print(f"\n  <<< O CPF deve ser um valor numerico valido! >>> \n")
                    sleep(3)
                    os.system(osclear)
                    print(f"""
************* Consulta de Pessoas Cadastradas ************
                Informe os dados solicitados 
\n\n    
    Selecione a informacao para buscar: 
      - N para buscar pelo nome
      - C para buscar pelo CPF 
      - F para finalizar consulta
                      
    Informe a opcao desejada:  {opcao}""")
                    continue
                else:
                    break
        elif opcao.upper() == "N":
            while True:
                nome =  input("\n    Informe o nome a ser buscado:    ")
                nome_check = checa_nome(nome)
                if nome_check == 1:
                    print(f"\n  <<< Digite o nome da pessoa! >>> \n")
                    sleep(3)
                    os.system(osclear)
                    print(f"""
************* Consulta de Pessoas Cadastradas ************
                Informe os dados solicitados 
\n\n    
    Selecione a informacao para buscar: 
      - N para buscar pelo nome
      - C para buscar pelo CPF 
      - F para finalizar consulta
                      
    Informe a opcao desejada:  {opcao}""")
                    continue
                else:
                    break
        elif opcao.upper() == "F":
            print("\n   >>> Saindo do menu de consulta de cadastro...")
            sleep(2)
            break
        else:
            print(f"\n  <<< Opcao invalida - selecione N, C ou F >>> \n")
            sleep(3)
            continue
        
        with open("aula 08/cadastro.csv","r") as arquivo:
            conteudo = csv.reader(arquivo, delimiter=";")  # generator - a variavel conteudo se torna
            found = False
            resultado_busca = []
            for linha in conteudo:
                if linha[0] == "CPF":
                    continue
                if (opcao.upper() == "C" and linha[0] == nCPF) or (opcao.upper() == "N" and linha[1] == nome):
                    resultado_busca = linha
                    # print(f"Esse e o resultado da busca:  {resultado_busca}")
                    found = True
                    break
            if opcao.upper() == "C" and found == False:
                print(f"\n  >>> Nao encontrado - o CPF {nCPF} nao esta cadastrado!")
                continuar = input("\n\n  >>>  Aperte ENTER para continuar...")
            elif opcao.upper() == "N" and found == False:
                print(f"\n  >>> Nao encontrado - o nome '{nome}' nao esta cadastrado!")
                continuar = input("\n\n  >>>  Aperte ENTER para continuar...")
            else:
                print(f"""\n\n   ********************************************
   >>>>>>>>   Resultado da consulta:   <<<<<<<<
   ********************************************\n
     -  CPF:          {resultado_busca[0]} 
     -  Nome:         {resultado_busca[1]} 
     -  Idade:        {resultado_busca[2]} 
     -  Sexo:         {resultado_busca[3]} 
     -  Cidade:       {resultado_busca[4]}
   ********************************************""")
                continuar = input("\n\n  >>>  Aperte ENTER para continuar...")
                
            


def deleta():
    nome = ""
    nCPF = ""
    while True:
        os.system(osclear)
        print("""
*************** Excluir Pessoas Cadastradas **************
                Informe os dados solicitados 
\n    """)
        opcao = input("""    Selecione a informacao para selecionar: 
      - N para excluir pelo nome
      - C para excluir pelo CPF 
      - F para finalizar exclusao
                      
    Informe a opcao desejada:  """)
        if opcao.upper() == "C":
            while True:
                nCPF =  input("\n    Informe o CPF a ser excluido:    ")
                cpf_check = checa_CPF(nCPF)
                if cpf_check == 1:
                    print(f"\n  <<< O valor {nCPF} nao e valido como CPF! >>> \n")
                    sleep(3)
                    os.system(osclear)
                    print(f"""
*************** Excluir Pessoas Cadastradas **************
                Informe os dados solicitados 
\n\n    
    Selecione a informacao para selecionar: 
      - N para excluir pelo nome
      - C para excluir pelo CPF 
      - F para finalizar exclusao
                      
    Informe a opcao desejada:  {opcao}""")
                    continue
                elif cpf_check == 2 or nCPF == "0":
                    print(f"\n  <<< O CPF deve ser um valor numerico valido! >>> \n")
                    sleep(3)
                    os.system(osclear)
                    print(f"""
*************** Excluir Pessoas Cadastradas **************
                Informe os dados solicitados 
\n\n    
    Selecione a informacao para selecionar: 
      - N para excluir pelo nome
      - C para excluir pelo CPF 
      - F para finalizar exclusao
                      
    Informe a opcao desejada:  {opcao}""")
                    continue
                else:
                    break
        elif opcao.upper() == "N":
            while True:
                nome =  input("\n    Informe o nome a ser excluido:    ")
                nome_check = checa_nome(nome)
                if nome_check == 1:
                    print(f"\n  <<< Digite o nome da pessoa! >>> \n")
                    sleep(3)
                    os.system(osclear)
                    print(f"""
*************** Excluir Pessoas Cadastradas **************
                Informe os dados solicitados 
\n\n    
    Selecione a informacao para selcionar: 
      - N para excluir pelo nome
      - C para excluir pelo CPF 
      - F para finalizar exclusao
                      
    Informe a opcao desejada:  {opcao}""")
                    continue
                else:
                    break
        elif opcao.upper() == "F":
            print("\n   >>> Saindo do menu de exclusao de cadastro...")
            sleep(2)
            break
        else:
            print(f"\n  <<< Opcao invalida - selecione N, C ou F >>> \n")
            sleep(3)
            continue
        
        with open("aula 08/cadastro.csv","r") as arquivo, open("aula 08/cadastro.edit.csv","w+") as novo_arq:
            conteudo = csv.reader(arquivo, delimiter=";")  # generator - a variavel conteudo se torna
            found = False
            resultado_busca = []
            temporary = []
            for linha in conteudo:
                if (opcao.upper() == "C" and linha[0] != nCPF) or (opcao.upper() == "N" and linha[1] != nome):
                    destino = csv.writer(novo_arq, delimiter=";")  # generator - a variavel conteudo se torna
                    temporary = linha
                    destino.writerow(temporary)
                    # print(f"Esse e o resultado da busca:  {resultado_busca}")
                else:
                    found = True    
                    resultado_busca = linha
            if opcao.upper() == "C" and found == False:
                print(f"\n  >>> Nao encontrado - o CPF {nCPF} nao estava cadastrado!")
                os.remove("aula 08/cadastro.edit.csv")
                continuar = input("\n\n  >>>  Aperte ENTER para continuar...")
            elif opcao.upper() == "N" and found == False:
                print(f"\n  >>> Nao encontrado - o nome '{nome}' nao estava cadastrado!")
                os.remove("aula 08/cadastro.edit.csv")
                continuar = input("\n\n  >>>  Aperte ENTER para continuar...")
            else:
                print(f"""\n\n   ********************************************
   >>>>>>>   Entrada a ser excluida:   <<<<<<<<
   ********************************************\n
     -  CPF:          {resultado_busca[0]} 
     -  Nome:         {resultado_busca[1]} 
     -  Idade:        {resultado_busca[2]} 
     -  Sexo:         {resultado_busca[3]} 
     -  Cidade:       {resultado_busca[4]}
   ********************************************""")
                confirma = input("\n\n  >>>  Confirma a exclusao ?  S confirma, senao cancela  ")
                if confirma.upper() == "S":
                    os.remove("aula 08/cadastro.csv")
                    os.rename("aula 08/cadastro.edit.csv","aula 08/cadastro.csv")
                    del_status = "excluida"
                else:
                    os.remove("aula 08/cadastro.edit.csv")
                    del_status = "nao excluida"
                print(f"\n   >>>  Entrada {del_status} do cadastro!")
                continuar = input("\n\n  >>>  Aperte ENTER para continuar...")

                
def menu():
    while True:
        os.system(osclear)
        print("""
**********************************************************
******************  Sistema de Cadastro  *****************
**********************************************************\n
        Opcoes disponiveis:
            A - Cadastrar novos registros
            C - Consultar cadastro
            E - Excluir registros do cadastro\n
            F - Sair do sistema\n""")
        opcao = input("        Digite a opcao desejada:  ")
        if opcao.upper() == "A":
            cadastra()
            continue
        elif opcao.upper() == "C":
            consulta()
            continue
        elif opcao.upper() == "E":
            deleta()
            continue
        elif opcao.upper() == "F":
            print("\n\n   >>> Saindo do sistema....\n")
            break
        else:
            print(f"\n\n   <<< Opcao {opcao} e invalida - selecione entre A, C, E ou F >>>")
            sleep(3)
            continue




menu()


# cadastra()
# deleta()
# consulta()



# while True:
#     os.system(osclear)
#     print("""
# **************** Cadastro de Pessoas ***************
#       Digite os dados pessoais solicitados 
#   (ou digite 0 no campo CPF para sair do programa)
#  """)
#     nCPF =  input("  CPF:       ")
#     if nCPF == "0":
#         print("\n Encerrando o programa...")
#         break

#     nome =  input("  Nome:      ")
#     idade = input("  Idade:     ")
#     sexo =  input("  Sexo:      ")
#     ender = input("  Endereco:  ")

#     if cabecalho == "N":
#         with open("aula 05/cadastro.csv","w") as arquivo:
#             linha = csv.writer(arquivo, delimiter=";")
#             minha_linha = ["CPF","Nome","Idade","Sexo","Endereco"]
#             linha.writerow(minha_linha)
#         cabecalho = "Y"

#     with open("aula 05/cadastro.csv","a",newline="") as arquivo:
#         linha = csv.writer(arquivo, delimiter=";")
#         minha_linha = [nCPF,nome,idade,sexo,ender]
#         linha.writerow(minha_linha)





