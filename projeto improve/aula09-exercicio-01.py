# Utilizando todo o conteudo que aprendeu no treinamento, crie uma aplicação capaz de simular o 
# funcionamento de um caixa eletrônico. O caixa deverá possuir as seguintes funções:
# - Registrar novos clientes.
# - Fazer login dos clientes.
# - Checar o saldo dos clientes.
# - Realizar depósitos.
# - Realizar saques.

# Faça o uso de Classes, funções e tratamentos de erro para otimizar seu código.

# - Clientes podem ser criados a partir de uma classe.
# - Informações dos clientes, como nome, senha e saldo, podem ser armazenadas em um banco de dados.
# - Podemos criar funções para tomar as ações do caixa eletrônico.
# - Podemos fazer uso de diversos arquivos e diretórios pra segmentar o nosso código.
# - Utilizar um interpretador prórprio para a aplicação, usando um ambiente virtual.

# =================================================================================================
# 1° - Criar um ambiente virtual para o exercicio.
# 2° - Criar um arquivo pra lidar com o banco de dados.
# 3° - Criar as tabelas no DB, uma pra armazenar as informações do cliente (nome, idade, saldo)
# e outra pra armazenar os usuarios/senha dos clientes.
# 4° - Criar um arquivo pra armazenar a classes utilizada no programa.
# 5° - Criar um arquivo que conterá as funções executadas pelo programa (registrar, deposito, saque).
# 6° - No meu arquivo principal, eu criariaria um menu para executar o programa.
# 7° - Corrigir valores repetidos sendo adicionados na primeira execução.
# 8° - Fazer um tratamento de erros em todo o programa.
# 9° - Melhorar a qualidade de uso do programa para o cliente. (Melhorar a comunicação)
# 10° - Criar um arquivo contendo os requerimentos para a aplicação.

#================================== Initials go here ======================================

# 1 # Initial setups #############################################################

# 1.1 # Imports ##################################################################
import os, sqlite3
from lib import operation
from time import sleep


# 1.2 # Global Variables #########################################################
if os.name == "nt":
    osclear = "cls"
else:
    osclear = "clear"

# 1.3 # Inicia conexao com database #############################################

conexao = sqlite3.connect("data/bank.db")  # Cria uma conexao com o banco de dados
cursor = conexao.cursor()  # Utilizado para editar o banco

#================================== Code starts here ======================================

initial_admin = operation.set_initial()

# print(f"Initial run:  {initial_admin}")
# sleep(5)

if initial_admin == True:
    novo_admin = operation.Conta()
    nova_conta = novo_admin.nova_conta()
    # print(f" '{nova_conta}' ")
    print (f"\n   >>> Nova conta admin:  '{nova_conta}'")
    sleep(3)
    operation.attrib_admin(nova_conta)


# while True:
while True:
    logoff = "N"
    while True:
        os.system(osclear)
        usuario = operation.Conta()
        usercheck = usuario.validacao()
        if usercheck[0] == 0:
            while True:
                if usercheck[1] == 1:
                    opcao = operation.menu_admin()
                    if opcao == "C":
                        usuario.consulta_saldo()
                    elif opcao == "D":
                        usuario.deposito()
                    elif opcao == "S":
                        usuario.retirada()
                    elif opcao == "N":
                        usuario.nova_conta()
                    elif opcao == "A":
                        operation.attrib_admin()
                    else:
                        print("\n\n  Terminando o acesso ...")
                        sleep(2)
                        logoff = "S"
                        break
                else:
                    opcao = operation.menu_principal()
                    if opcao == "C":
                        usuario.consulta_saldo()
                    elif opcao == "D":
                        usuario.deposito()
                    elif opcao == "S":
                        usuario.retirada()
                    else:
                        print("\n\n  Terminando o acesso ...")
                        sleep(2)
                        logoff = "S"
                        break
        else:
            pass
        if logoff == "S":
            break




    
