import sqlite3, os, pyAesCrypt, secrets
from hashlib import sha3_512, shake_256
from time import sleep

bufferSize = 64*1024

conexao = sqlite3.connect("data/bank.db")  # Cria uma conexao com o banco de dados
cursor = conexao.cursor()  # Utilizado para editar o banco


# initial_admin = False


def cria_tabelas():

    cria_tabela1 = """  
CREATE TABLE IF NOT EXISTS contas (
conta integer primary key autoincrement,
nome text,
idade integer,
saldo real
)"""

    cria_tabela2 = """  
CREATE TABLE IF NOT EXISTS usersec (
conta integer primary key autoincrement,
hash1 text,
hash2 text,
adminid text
)"""

    cria_tabela3 = """  
CREATE TABLE IF NOT EXISTS duplsec (
n_count integer primary key autoincrement,
locator text,
applpwd text
)"""

    cria_tabela4 = """  
CREATE TABLE IF NOT EXISTS extsec (
conta integer primary key autoincrement,
password text
)"""


    try:
        cursor.execute(cria_tabela1)
    except sqlite3.OperationalError:
        pass

    try:
        cursor.execute(cria_tabela2)
    except sqlite3.OperationalError:
        pass

    try:
        cursor.execute(cria_tabela3)
    except sqlite3.OperationalError:
        pass


    if os.path.exists("data/secalt.db.aes") != True:

        conexts = sqlite3.connect("data/secalt.db")  # Cria uma conexao com o banco de dados
        scursor = conexts.cursor()  # Utilizado para editar o banco

        try:
            scursor.execute(cria_tabela4)
        except sqlite3.OperationalError:
            pass
        else:
            conexts.commit()
            conexts.close()

        masterkey = secrets.token_hex(128)
        adiciona_mast = f"INSERT INTO duplsec (locator, applpwd) VALUEs ('app', '{masterkey}')"
        cursor.execute(adiciona_mast)
        conexao.commit()

        pyAesCrypt.encryptFile("data/secalt.db", "data/secalt.db.aes", masterkey, bufferSize)
        os.remove("data/secalt.db")



def add_conta(nome:int, idade:int, deposito:float, senha:str, admin:str=""):
    pega_mast = f"SELECT applpwd FROM duplsec WHERE locator = 'app'"  
    cursor.execute(pega_mast)
    key = cursor.fetchone()

    pyAesCrypt.decryptFile("data/secalt.db.aes", "data/secalt.db", key[0], bufferSize)
    sleep(1)
    os.remove("data/secalt.db.aes")
    while os.access("data/secalt.db", os.W_OK) != True:
        sleep(1)
    conexts = sqlite3.connect("data/secalt.db")  # Cria uma conexao com o banco de dados
    scursor = conexts.cursor()  # Utilizado para editar o banco

    s_hash = sha3_512(senha.encode('ascii')).hexdigest()
    l_hash = shake_256(senha.encode('ascii')).hexdigest(255)
    adiciona_conta = f"INSERT INTO contas (nome, idade, saldo) VALUES ('{nome}', {idade}, {deposito})"
    cursor.execute(adiciona_conta)
    adiciona_sec = f"INSERT INTO usersec (hash1, hash2, adminid) VALUES ('{s_hash}', '{l_hash}', '{admin}')"
    cursor.execute(adiciona_sec)
    conexao.commit()

    adiciona_ext = f"INSERT INTO extsec (password) VALUES ('{senha}')"
    while True:
        try:
            scursor.execute(adiciona_ext)
        except sqlite3.OperationalError:
            continue
        else:
            conexts.commit()
            conexts.close()
            break
     
    pega_conta = f"SELECT conta FROM contas WHERE nome = '{nome}' and idade = {idade} and saldo = {deposito}"  
    cursor.execute(pega_conta)
    numero_conta = cursor.fetchone()

    sleep(1)
    pyAesCrypt.encryptFile("data/secalt.db", "data/secalt.db.aes", key[0], bufferSize)
    sleep(1)
    os.remove("data/secalt.db")
    while os.path.exists("data/secalt.db") == True:
        sleep(1)


    if numero_conta == "":
        resposta = [1,0]
    else:
        resposta = [0,numero_conta]
    return resposta


def validar(conta: int, senha: str):
    test_hash_s = sha3_512(senha.encode('ascii')).hexdigest()
    test_hash_l = shake_256(senha.encode('ascii')).hexdigest(255)
    seleciona_conta = f"SELECT hash1, hash2, adminid FROM usersec WHERE conta = {conta}"
    cursor.execute(seleciona_conta)
    conteudo = cursor.fetchone()
    if conteudo[0] == test_hash_s and conteudo[1] == test_hash_l:
        if conteudo[2] == "admin":
            resposta = [0,1]
        else:
            resposta = [0,0]
        return resposta
    else:
        resposta = [1,0]
        return resposta


def conta_existe(conta: int):
    seleciona_conta = f"SELECT nome FROM contas WHERE conta = {conta}"
    cursor.execute(seleciona_conta)
    conteudo = cursor.fetchone()
    if conteudo != "":
        resposta = 0
    else:
        resposta = 1
    return resposta


def ver_saldo(conta: int):
    seleciona_conta = f"SELECT nome, saldo FROM contas WHERE conta = {conta}"
    cursor.execute(seleciona_conta)
    conteudo = cursor.fetchone()
    resposta = [0, conteudo[0], conteudo[1]]
    return resposta


def deposito(conta: int, valor: float):
    seleciona_conta = f"SELECT saldo FROM contas WHERE conta = {conta}"
    cursor.execute(seleciona_conta)
    conteudo = cursor.fetchone()
    # print(f"conteudo: '{conteudo}'  //  valor: '{valor}'")
    # sleep(2)
    saldo = conteudo[0]
    saldo += valor
    atualiza_tabela = f'UPDATE contas SET saldo = {saldo} WHERE conta = {conta}'    
    cursor.execute(atualiza_tabela)
    conexao.commit()
    seleciona_conta = f"SELECT saldo FROM contas WHERE conta = {conta}"
    cursor.execute(seleciona_conta)
    conteudo = cursor.fetchone()
    resposta = [0,conteudo[0]]
    return resposta


def saque(conta: int, valor: float):
    seleciona_conta = f"SELECT saldo FROM contas WHERE conta = {conta}"
    cursor.execute(seleciona_conta)
    conteudo = cursor.fetchone()
    # print(f"{conteudo}")
    # print(f"{conteudo[0]}")
    saldo = float(conteudo[0])
    saldo -= float(valor)
    atualiza_tabela = f'UPDATE contas SET saldo = {saldo} WHERE conta = {conta}'    
    cursor.execute(atualiza_tabela)
    conexao.commit()
    seleciona_conta = f"SELECT saldo FROM contas WHERE conta = {conta}"
    cursor.execute(seleciona_conta)
    conteudo = cursor.fetchone()
    resposta = [0,conteudo[0]]
    return resposta


def checa_initial():
    initial_admin = True
    checa_admin = f"SELECT adminid FROM usersec"
    cursor.execute(checa_admin)
    conteudo = cursor.fetchall()
    # print(conteudo)
    for x in conteudo:
        # print(f"Checa admin: '{x}':'{x[0]}'")
        if x[0] == "admin":
            initial_admin = False
            break
        elif initial_admin != False:
            pass
    return initial_admin


def set_admin(conta: int):
    seleciona_conta = f"SELECT nome FROM contas WHERE conta = {conta}"
    cursor.execute(seleciona_conta)
    conteudo = cursor.fetchone()
    if conteudo == "":
        return 1
    else:
        atualiza_admin = f'UPDATE usersec SET adminid = "admin" WHERE conta = {conta}'    
        cursor.execute(atualiza_admin)
        conexao.commit()
        seleciona_conta = f"SELECT adminid FROM usersec WHERE conta = {conta}"
        cursor.execute(seleciona_conta)
        conteudo = cursor.fetchone()
        if conteudo == "admin":
            return 0
        else:
            return 2

