from lib import dbgeneral
import os
from time import sleep, localtime, strftime
from getpass import getpass
from hashlib import sha3_512

if os.name == "nt":
    osclear = "cls"
else:
    osclear = "clear"

min_dep = 10.00

# initial_admin = True


def qual_saudacao():
    horario_atual = localtime()
    horario = strftime("%H:%M:%S", horario_atual)
    if horario < "12:00:00":
        return "Bom dia"
    elif horario < "18:00:00":
        return "Boa tarde"
    else:
        return "Boa noite"


def isfloat(test_value):
    try: 
        float(test_value)
    except ValueError: 
        return False
    return True


def checa_nome(nome_info):
    if nome_info == "":
        return 1
    else:
        return 0


def checa_idade(idade_info):
    if idade_info == "":
        return 1
    elif idade_info.isnumeric() == False:
        return 2
    elif int(idade_info) < 0:
        return 3    
    elif int(idade_info) > 130:
        return 4
    else:
        return 0


def checa_valor(valor_info):
    global min_dep
    if valor_info == "":
        return 1
    elif isfloat(valor_info) == False:
        return 2
    elif float(valor_info) < min_dep:
        return 3
    else:
        return 0


def checa_senha(senha_info):
    if senha_info == "":
        return 1
    elif len(senha_info) < 6:
        return 2
    elif len(senha_info) > 20:
        return 3
    else:
        minusc = False 
        maiusc = False
        numero = False
        for x in senha_info:
            if x in "abcdefghijklmnopqrstuvwxyz":
                minusc = True
                break
        for x in senha_info:
            if x in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
                maiusc = True
                break
        for x in senha_info:
            if x in "0123456789":
                numero = True
                break
        if minusc != True or maiusc != True or numero != True:
            return 4
        else:
            return 0


def checa_csenha(senha_info, contrasenha_info):
    if senha_info != contrasenha_info:
        return 5
    else:
        return 0


def checa_conta(conta_info):
    if conta_info == "":
        return 1
    elif conta_info.isnumeric() == False:
        return 2
    else:
        return 0


def checa_opcao(opcao_info, admin_flag):
    opcao_info = opcao_info.upper()
    if admin_flag == 0:
        if opcao_info == "C" or opcao_info == "D" or opcao_info == "S" or opcao_info == "E":
            return 0
        else:
            return 1
    else:
        if opcao_info == "C" or opcao_info == "D" or opcao_info == "S" or opcao_info == "E" or \
         opcao_info == "N" or opcao_info == "A":
            return 0
        else:
            return 1


def set_initial():
    dbgeneral.cria_tabelas()
    initial_admin = dbgeneral.checa_initial()
    return(initial_admin)

def attrib_admin(conta=""):
    while True:
        os.system(osclear)
        if conta =="":
            conta = input("\n\n Digite o numero da conta a ser promovida a admin:  ")
        c_conta = checa_conta(str(conta))
        if c_conta == 0:
            t_conta = dbgeneral.conta_existe(int(conta))
            if t_conta == 0:
                dbgeneral.set_admin(int(conta))
                break
            else:
                print(f"\n\n  <<< Conta '{conta}' nao esta cadastrada! >>>")
                continuar = input("\n\n  >>> Deseja tentar novamente ? Tecle S para tentar, senao sai...  ")
                sleep(2)
                if continuar.upper() == "S":
                    continue
                else:
                    break
        else:
            print(f"\n\n  <<< Conta '{conta}' e invalida! >>>")
            continuar = input("\n\n  >>> Deseja tentar novamente ? Tecle S para tentar, senao sai...  ")
            sleep(2)
            if continuar.upper() == "S":
                continue
            else:
                break


def menu_principal():
    while True:
        os.system(osclear)
    # *************************************************************
    #
    #
    #                  Operacoes de Conta Corrente
    #    
    #         Escolha entre as operacoes disponiveis:
    #
    #             C  -  Consulta de saldo
    #    
    #             D  -  Deposito
    #    
    #             S  -  Saque de conta corrente
    #    
    #             E  -  Terminar acesso
    #
    #
    #
    #         Selecione a operacao desejada:     |
    #
    #
    #
    #  >>>
    #
        print(f"""
      *************************************************************\n\n     
                       Operacoes de Conta Corrente\n
              Escolha entre as operacoes disponiveis:\n
                  C  -  Consulta de saldo\n
                  D  -  Deposito\n
                  S  -  Saque de conta corrente\n
                  E  -  Terminar acesso\n\n\n""")
        opcao = input("              Selecione a operacao desejada:     ")
        c_opcao = checa_opcao(opcao,0)
        if c_opcao == 0:
            opcao = opcao.upper()
            return opcao
        else:
            print(f"\n\n\n   Operacao invalida - selecione entre C, D, S ou E")
            sleep(3)
            continue


def menu_admin():
    while True:
        os.system(osclear)
    # *************************************************************
    #
    #
    #                  Operacoes de Conta Corrente
    #    
    #         Escolha entre as operacoes disponiveis:
    #
    #             C  -  Consulta de saldo
    #    
    #             D  -  Deposito
    #    
    #             S  -  Saque de conta corrente
    #    
    #             E  -  Terminar acesso
    #
    #         Operacoes administrativas:
    #
    #             N  -  Cadastrar nova conta
    #
    #             A  -  Promover conta para administrador
    #
    #         Selecione a operacao desejada:     |
    #
    #  >>>
    #
        print(f"""
      *************************************************************\n\n     
                       Operacoes de Conta Corrente\n
              Escolha entre as operacoes disponiveis:\n
                  C  -  Consulta de saldo\n
                  D  -  Deposito\n
                  S  -  Saque de conta corrente\n
                  E  -  Terminar acesso\n
              Operacoes administrativas:\n
                  N  -  Cadastrar nova conta\n
                  A  -  Promover conta para administrador\n""")        
        opcao = input("              Selecione a operacao desejada:     ")
        c_opcao = checa_opcao(opcao,1)
        if c_opcao == 0:
            opcao = opcao.upper()
            return opcao
        else:
            print(f"\n\n\n   Operacao invalida - selecione entre C, D, S, E, N ou A")
            sleep(3)
            continue



class Tela:
    def __init__(self):
        self.none = None

    def cadastra_conta(self, vars, nome="", idade=None, valor=None):
        os.system(osclear)
    # *************************************************************
    #
    #                 Cadastro de Novas Contas
    #    
    #    
    #        Cadastrar novo cliente:
    #    
    #           Nome:          |
    #           Idade:         |
    #          
    #           Deposito inicial (min. R$10.00)   |
    #
    #        Agora digite uma senha (minimo 6 e maximo 20 caracteres)
    #           - use MAIUSCULAS, minusculas e numeros (0-9)
    #
    #           Senha                |
    #
    #           Confirme a senha     | 
    #
    # >>>
        print(f"""
    *************************************************************\n
                    Cadastro de Novas Contas\n\n
             Cadastrar novo cliente:\n""")
        if vars > 0:
            print(f"                Nome:          {nome}")
        if vars > 1:
            print(f"                Idade:         {idade}\n")
        if vars > 2:
            print(f"""                Deposito inicial (min. R${min_dep}) R${valor}\n
             Agora digite uma senha (minimo 6 e maximo 20 caracteres)
                - use MAIUSCULAS, minusculas e numeros (0-9)\n""")
        # if vars > 3:
        #     print(f"    #             Senha              {senha}\n")

    def cad_error(self, vars, num_error):
        err_message = ""
    # *************************************************************
    #
    #                 Cadastro de Novas Contas
    #    
    #
    #        Cadastrar novo cliente:
    #    
    #           Nome:          |
    #           Idade:         |
    #          
    #           Deposito inicial (min. R$10.00)   |
    #
    #        Agora digite uma senha (minimo 6 e maximo 20 caracteres)
    #           - use MAIUSCULAS, minusculas e numeros (0-9)
    #
    #           Senha                |
    #
    #           Confirme a senha     | 
    #
    # >>>
        if vars == 0:
            if num_error == 1:
                err_message = "Nome nao pode ficar vazio!"
            print(f"\n\n\n\n\n\n\n\n\n\n\n   <<< {err_message} >>>")
        elif vars == 1:
            if num_error == 1:
                err_message = "Idade nao pode ficar vazia!"
            elif num_error == 2:
                err_message = "Idade tem que ser numerica!"
            elif num_error == 3:
                err_message = "Valor invalido para Idade - tem que ser maior que 0!"
            elif num_error == 4:
                err_message = "Valor invalido para Idade - valor maximo aceito e 130!"
            print(f"\n\n\n\n\n\n\n\n\n\n   <<< {err_message} >>>")
        elif vars == 2:
            if num_error == 1:
                err_message = "Valor do deposito inicial nao pode ficar vazio!"
            elif num_error == 2:
                err_message = "Valor do deposito inicial tem que ser numerico!"
            elif num_error == 3:
                err_message = "Valor invalido para deposito inicial - tem que ser maior que R$10.00!"
            print(f"\n\n\n\n\n\n\n\n   <<< {err_message} >>>")
        elif vars == 3:
            if num_error == 1:
                err_message = "Senha nao pode ficar vazia!"
            elif num_error == 2:
                err_message = "Senha nao pode ser menor que 6 caracteres!"
            elif num_error == 3:
                err_message = "Senha nao pode ser maior que 20 caracteres!"
            elif num_error == 4:
                err_message = "Senha tem que ter letras MAIUSCULAS, minusculas e numeros (0-9)!"
            print(f"\n\n\n   <<< {err_message} >>>")
        elif vars == 4:
            if num_error == 1:
                err_message = "As senhas informadas nao sao iguais! Digite novamente!"
            print(f"\n   <<< {err_message} >>>")
        else:
            pass
            # print(f"\n\n\n\n\n\n\n\n\n   >>> ")
        sleep(3)

    def valida_conta(self, vars, conta=""):
        os.system(osclear)
    # *************************************************************
    #
    #
    #                  Acesso a Conta Corrente
    #    
    #    
    #         Digite o numero da sua conta e sua senha:
    #
    #    
    #                   Conta        | 
    #
    #                   Senha        |
    #
    #
    #
    #
    #
    #  >>>
    #
        print(f"""
     *************************************************************\n\n    
                      Acesso a Conta Corrente\n\n
             Digite o numero da sua conta e sua senha:\n\n""")
        if vars > 0:
            print(f"                        Conta        {conta}")
        if vars > 1:
            print(f"\n                        Senha        ")

    def val_error(self, vars, num_error):
        err_message = ""
    # *************************************************************
    #
    #
    #                  Acesso a Conta Corrente
    #    
    #    
    #         Digite o numero da sua conta e sua senha:
    #
    #    
    #                   Conta        | 
    #
    #                   Senha        |
    #
    #
    #
    #
    #
    #  >>>
    #
        if vars == 0:
            if num_error == 1:
                err_message = "Numero da conta precisa ser informado!"
            elif num_error == 2:
                err_message = "Conta tem que ser numerica!"
            print(f"\n\n\n\n\n\n\n   <<< {err_message} >>>")
        elif vars == 1:
            if num_error == 1:
                err_message = "Senha nao pode ficar vazia!"
            print(f"\n\n\n\n\n   <<< {err_message} >>>")
        elif vars == 2:
            if num_error == 1:
                err_message = "Conta/Senha invalida!"
            elif num_error == 2:
                err_message = "Saindo: acesso de conta corrente..."
            print(f"\n\n\n   <<< {err_message} >>>")
        else:
            pass
        sleep(3)

    def saque_conta(self, nome_info="", saldo=0.0):
        os.system(osclear)
    # *************************************************************
    #
    #
    #                  Saque de Conta Corrente
    #    
    #    
    #         nome
    #
    #    
    #         O saldo atual da sua conta e:       R$|
    #
    #         Informe o valor que deseja sacar:   R$|
    #
    #
    #         Saque realizado, seu saldo atual e  R$|
    #
    #
    #  >>>
    #
        print(f"""
      *************************************************************\n\n
                         Saque de Conta Corrente\n\n
              {nome_info}\n\n
              O saldo atual da sua conta e:       R${saldo}\n""")

    def saq_error(self, vars, num_error):
        err_message = ""
    # *************************************************************
    #
    #
    #                  Saque de Conta Corrente
    #    
    #    
    #         nome
    #
    #    
    #         O saldo atual da sua conta e:       R$|
    #
    #         Informe o valor que deseja sacar:   R$|
    #
    #
    #         Saque realizado, seu saldo atual e  R$|
    #
    #
    #  >>>
    #
        if vars == 0:
            if num_error == 1:
                err_message = "Valor do saque nao pode ficar vazio!"
            elif num_error == 2:
                err_message = "Valor do saque tem que ser numerico!"
            elif num_error == 3:
                err_message = "Saldo insuficiente!"
            elif num_error == 4:
                err_message = "Erro nao especificado - tente novamente!"
            print(f"\n\n\n\n\n   <<< {err_message} >>>")
        else:
            pass
        sleep(3)

    def deposito_conta(self, nome_info="", saldo=0.0):
        os.system(osclear)
    # *************************************************************
    #
    #
    #                 Deposito para Conta Corrente
    #    
    #    
    #         nome
    #
    #    
    #         O saldo atual da sua conta e:       R$|
    #
    #         Informe o valor a depositar:        R$|
    #
    #
    #         Deposito realizado, saldo atual e   R$|
    #
    #
    #  >>>
    #
        print(f"""
      *************************************************************\n\n
                      Deposito para Conta Corrente\n\n
              {nome_info}\n\n
              O saldo atual da sua conta e:       R${saldo}\n""")

    def dep_error(self, vars, num_error):
    # *************************************************************
    #
    #
    #                 Deposito para Conta Corrente
    #    
    #    
    #         nome
    #
    #    
    #         O saldo atual da sua conta e:       R$|
    #
    #         Informe o valor a depositar:        R$|
    #
    #
    #         Deposito realizado, saldo atual e   R$|
    #
    #
    #  >>>
    #
        if vars == 0:
            if num_error == 1:
                err_message = "Valor do deposito nao pode ficar vazio!"
            elif num_error == 2:
                err_message = "Valor do deposito tem que ser numerico!"
            elif num_error == 4:
                err_message = "Erro nao especificado - tente novamente!"
            print(f"\n\n\n\n\n   <<< {err_message} >>>")
        sleep(3)



class Conta:

    def __init__(self):
        self.conta = 0
        self.nome1 = ""
        self.saldo = 0.0
        self.__isadmin__ = False
        self.__isvalidated__ = False

    def nova_conta(self):
        cadtela = Tela()
        os.system(osclear)
        print(f"""
    *************************************************************\n
                    Cadastro de Novas Contas\n\n
             Cadastrar novo cliente:\n""")
        while True:        
            nome = input(f"                Nome:          ")
            c_nome = checa_nome(nome)
            if c_nome != 0:
                cadtela.cad_error(0,c_nome)
                cadtela.cadastra_conta(0)
                continue
            else:
                break
        while True:
            idade = input(f"                Idade:         ")
            c_idade = checa_idade(idade)
            if c_idade != 0:
                cadtela.cad_error(1,c_idade)
                cadtela.cadastra_conta(1, nome)
                continue
            else:
                break
        while True:           
            valor = input(f"\n                Deposito inicial (min. R${min_dep:.2f}) R$")
            c_valor = checa_valor(valor)
            if c_valor != 0:
                cadtela.cad_error(2,c_valor)
                cadtela.cadastra_conta(2, nome, idade)
                continue
            else:
                break
        print("""
             Agora digite uma senha (minimo 6 e maximo 20 caracteres)
                - use MAIUSCULAS, minusculas e numeros (0-9)\n""")
        while True:
            senha = getpass(prompt="                Senha                ")       
            c_senha = checa_senha(senha)
            if c_senha != 0:
                cadtela.cad_error(3,c_senha)
                cadtela.cadastra_conta(3, nome, idade, valor)
                continue
            else:   
                contrasenha = getpass(prompt="\n                Confirme a senha     ")
                # print(f"\n\n{sha3_512(senha.encode('ascii')).hexdigest()}")
                # print(f"\n{sha3_512(contrasenha.encode('ascii')).hexdigest()}")
                c_csenha = checa_csenha(senha, contrasenha)
                if c_csenha != 0:
                    cadtela.cad_error(4,c_csenha)
                    cadtela.cadastra_conta(3, nome, idade, valor)
                    continue
                else:
                    break
        resposta = dbgeneral.add_conta(nome, idade, valor, senha)
        # print(f"Resposta: {resposta}")
        if resposta[0] == 0:
            num_conta = str(resposta[1][0]).rjust(5,' ')
            rsp_message = f"Cliente {nome} cadastrado, o numero da conta e : {num_conta}."
            print(f"\n   >>> {rsp_message} >>>")
            sleep(3)
            return resposta[1][0]

    def consulta_saldo(self):
        saudacao = qual_saudacao()
        if self.nome1 == "":
            resposta = dbgeneral.ver_saldo(self.conta)
            self.saldo = resposta[2]
            nome = resposta[1]
            self.nome1 = nome.split()[0]
    # *************************************************************
    #
    #
    #                      Saldo Conta Corrente
    #    
    #    
    #         Bom dia nome
    #
    #    
    #         O saldo atual da sua conta e:       R$| 
    #
    #
    #
    #
    #
    #
    #
    #  >>>   Tecle ENTER para continuar...
    #
        saldo = self.saldo
        nome1 = self.nome1
        os.system(osclear)
        print(f"""
      *************************************************************\n\n
                           Saldo Conta Corrente\n\n
              {saudacao} {nome1}\n\n
              O saldo atual da sua conta e:       R${saldo:.2f}\n\n\n\n\n\n\n""")
        continuar = input("       >>>   Tecle ENTER para continuar...  ")
        retorno = [0, saldo]

    def retirada(self):
        if self.nome1 == "":
            resposta = dbgeneral.ver_saldo(self.conta)
            self.saldo = resposta[2]
            nome = resposta[1]
            self.nome1 = nome.split()[0]
        saqtela = Tela()
    # *************************************************************
    #
    #
    #                  Saque de Conta Corrente
    #    
    #    
    #         nome
    #
    #    
    #         O saldo atual da sua conta e:       R$|
    #
    #         Informe o valor que deseja sacar:   R$|
    #
    #
    #         Saque realizado, seu saldo atual e  R$|
    #
    #
    #  >>>
    #
        os.system(osclear)
        saldo = self.saldo
        nome1 = self.nome1
        conta = self.conta
        print(f"""
      *************************************************************\n\n
                       Saque de Conta Corrente\n\n
              {nome1}\n\n
              O saldo atual da sua conta e:       R${saldo:.2f}\n""")
        while True:
            while True:
                saque = input("              Informe o valor que deseja sacar:   R$")
                c_saque = checa_valor(saque)
                if c_saque == 1 or c_saque == 2:
                    saqtela.saq_error(0, c_saque)
                    saqtela.saque_conta(0,nome1,saldo)
                    continue
                else:
                    saque = float(saque)
                    # print(saque)
                    break
            if float(saldo) < saque:
                saqtela.saq_error(0, 3)    
                continuar = input("\n   >>>  Para tentar novo saque tecle S, senao sai...  ")
                sleep(1)
                saqtela.saque_conta(0,nome1,saldo)
                continue
            else:
                novo_saldo = dbgeneral.saque(conta,saque)
                # print(f"Saldo anterior: {saldo}  //  Novo saldo: {novo_saldo}")
                if novo_saldo[1] == saldo:
                    saqtela.saq_error(0, 4)
                    continuar = input("\n   >>>  Para tentar novo saque tecle S, senao sai...  ")
                    if continuar.upper() == "S":
                        sleep(1)
                        saqtela.saque_conta(0,nome1,saldo)
                        continue
                    else:
                        break
                else:
                    self.saldo = novo_saldo[1]
                    print(f"\n\n              Saque realizado, seu saldo atual e  R${novo_saldo[1]:.2f}")
                    sleep(2)
                    break

    def deposito(self):
        if self.nome1 == "":
            resposta = dbgeneral.ver_saldo(self.conta)
            self.saldo = resposta[2]
            nome = resposta[1]
            self.nome1 = nome.split()[0]
        deptela = Tela()
    # *************************************************************
    #
    #
    #                 Deposito para Conta Corrente
    #    
    #    
    #         nome
    #
    #    
    #         O saldo atual da sua conta e:       R$|
    #
    #         Informe o valor a depositar:        R$|
    #
    #
    #         Deposito realizado, saldo atual e   R$|
    #
    #
    #  >>>
    #
        saldo = self.saldo
        nome1 = self.nome1
        conta = self.conta
        os.system(osclear)
        print(f"""
      *************************************************************\n\n
                      Deposito para Conta Corrente\n\n
              {nome1}\n\n
              O saldo atual da sua conta e:       R${saldo:.2f}\n""")
        while True:
            while True:
                depos = input("              Informe o valor a depositar:        R$")
                c_depos = checa_valor(depos)
                if c_depos == 1 or c_depos == 2:
                    deptela.dep_error(0, c_depos)
                    deptela.deposito_conta(0,nome1,saldo)
                    continue
                else:
                    break
            novo_saldo = dbgeneral.deposito(conta,float(depos))
            if novo_saldo[1] == saldo:
                deptela.saq_error(0, 4)
                continuar = input("\n   >>>  Para tentar novo deposito tecle S, senao sai...  ")
                if continuar.upper() == "S":
                    sleep(1)
                    deptela.saque_conta(0,nome1,saldo)
                    continue
                else:
                    break
            else:
                self.saldo = novo_saldo[1]
                print(f"\n\n              Deposito realizado, seu saldo atual e  R${novo_saldo[1]:.2f}")
                sleep(2)
                break

    def validacao(self):
    # *************************************************************
    #
    #
    #                  Acesso a Conta Corrente
    #    
    #    
    #         Digite o numero da sua conta e sua senha:
    #
    #    
    #                   Conta        | 
    #
    #                   Senha        |
    #
    #
    #
    #
    #
    #  >>>
    #
        valtela = Tela()
        print(f"""
     *************************************************************\n\n    
                      Acesso a Conta Corrente\n\n
             Digite o numero da sua conta e sua senha:\n\n""")       
        while True:
            while True:        
                conta = input(f"                        Conta        ")
                c_conta = checa_conta(conta)
                if c_conta != 0:
                    valtela.val_error(0, c_conta)
                    valtela.valida_conta(0)
                    continue
                else:
                    break
            while True:       
                senha = getpass(prompt="\n                        Senha        ")   
                c_senha = checa_senha(senha)
                if c_senha == 1:
                    valtela.val_error(1, c_senha)
                    valtela.valida_conta(1, conta)
                    continue
                else:
                    break
            resposta = dbgeneral.validar(conta, senha)
            if resposta[0] == 0:
                self.__isvalidated__ = True
                validated = 0
                self.conta = conta
                if resposta[1] == 1:
                    self.__isadmin__ = True
                    admin_status = 1        
                else:
                    admin_status = 0
                return [validated, admin_status]
            else:
                validated = 1
                admin_status = 0
                valtela.val_error(2, resposta)
                val_continua = input(f"\n  >>> Deseja tentar novamente ?  S para tentar, senao sai:  ")
                if val_continua.upper() == "S":
                    sleep(1)
                    valtela.valida_conta(0)
                    continue
                else:
                    valtela.valida_conta(2, conta)
                    valtela.val_error(2, 2)
                    sleep(2)
                    return [validated, admin_status]





