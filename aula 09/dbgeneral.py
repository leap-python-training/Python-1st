
conexao = sqlite3.connect("data/bank.db")  # Cria uma conexao com o banco de dados
cursor = conexao.cursor()  # Utilizado para editar o banco



def cria_tabelas():

    cria_tabela1 = """  
CREATE TABLE IF NOT EXISTS contas (
conta integer primary key autoincrement,
nome text,
idade integer,
saldo real
)"""

    cria_tabela2 = """  
CREATE TABLE IF NOT EXISTS usersec (
conta integer primary key autoincrement,
senha text
)"""

    try:
        cursor.execute(cria_tabela1)
    except sqlite3.OperationalError:
        pass

    try:
        cursor.execute(cria_tabela2)
    except sqlite3.OperationalError:
        pass



def add_conta(nome, idade, deposito, senha):
    adiciona_conta = f"INSERT INTO contas (nome, idade, saldo) VALUES ('{nome}', '{idade}', {deposito})"
    cursor.execute(adiciona_conta)
    adiciona_sec = f"INSERT INTO usersec (senha) VALUES ('{senha}')"
    cursor.execute(adiciona_sec)
    conexao.commit()
    pega_conta = f"SELECT conta FROM contas WHERE nome = '{nome} and idade = {idade} and saldo = {deposito}"  
    cursor.execute(pega_conta)
    numero_conta = cursor.fetchone()
    resposta = [0,numero_conta]
    return resposta


def validar(conta, senha):
    seleciona_conta = f"SELECT senha FROM usersec WHERE conta = {conta}"
    cursor.execute(seleciona_conta)
    conteudo = cursor.fetchone()
    if conteudo == senha:
        return 0
    else:
        return 1


def ver_saldo(conta):
    seleciona_conta = f"SELECT saldo FROM contas WHERE conta = {conta}"
    cursor.execute(seleciona_conta)
    conteudo = cursor.fetchone()
    resposta = [0,conteudo]
    return resposta



def deposito(conta, valor):
    seleciona_conta = f"SELECT saldo FROM contas WHERE conta = {conta}"
    cursor.execute(seleciona_conta)
    conteudo = cursor.fetchone()
    saldo = conteudo
    saldo += valor
    atualiza_tabela = f'UPDATE contas SET saldo = {saldo} WHERE conta = {conta}'    
    cursor.execute(atualiza_tabela)
    conexao.commit()
    seleciona_conta = f"SELECT saldo FROM contas WHERE conta = {conta}"
    cursor.execute(seleciona_conta)
    conteudo = cursor.fetchone()
    resposta = [0,conteudo]
    return resposta


def saque(conta, valor):
    seleciona_conta = f"SELECT saldo FROM contas WHERE conta = {conta}"
    cursor.execute(seleciona_conta)
    conteudo = cursor.fetchone()
    saldo = conteudo
    saldo -= valor
    atualiza_tabela = f'UPDATE contas SET saldo = {saldo} WHERE conta = {conta}'    
    cursor.execute(atualiza_tabela)
    conexao.commit()
    seleciona_conta = f"SELECT saldo FROM contas WHERE conta = {conta}"
    cursor.execute(seleciona_conta)
    conteudo = cursor.fetchone()
    resposta = [0,conteudo]
    return resposta


