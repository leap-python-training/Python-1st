import dbgeneral
import os
from time import sleep

if os.name == "nt":
    osclear = "cls"
else:
    osclear = "clear"

min_dep = 10.00

def checa_nome(nome_info):
    if nome_info == "":
        return 1
    else:
        return 0


def checa_idade(idade_info):
    if idade_info == "":
        return 1
    elif idade_info.isnumeric() == False:
        return 2
    elif idade_info < 0:
        return 3    
    elif idade_info > 130:
        return 4
    else:
        return 0


def checa_valor(valor_info):
    if valor_info == "":
        return 1
    elif valor_info.isnumeric() == False:
        return 2
    elif valor_info < min_dep:
        return 3
    else:
        return 0


def checa_senha(senha_info):
    if senha_info == "":
        return 1
    elif len(senha_info) < 6:
        return 2
    elif len(senha_info) > 20:
        return 3
    else:
        minusc = False 
        maiusc = False
        numero = False
        for x in senha_info:
            if x in "abcdefghijklmnopqrstuvwxyz":
                minusc = True
                break
        for x in senha_info:
            if x in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
                maiusc = True
                break
        for x in senha_info:
            if x in "0123456789":
                numero = True
                break
        if minusc != True or maiusc != True or numero != True:
            return 4
        else:
            return 0



def checa_csenha(senha_info, contrasenha_info):
    if senha_info != contrasenha_info:
        return 5
    else:
        return 0



class Tela:

    def __init__(self):
        self.none = None

    def cadastra_conta(self, vars, nome="", idade=None, valor=None, senha=""):
        os.system(osclear)
    # *************************************************************
    #
    #                 Cadastro de Novas Contas
    #    
    #    
    #        Cadastrar novo cliente:
    #    
    #           Nome:          |
    #           Idade:         |
    #          
    #           Deposito inicial (min. R$10.00)   |
    #
    #        Agora digite uma senha (minimo 6 e maximo 20 caracteres)
    #           - use MAIUSCULAS, minusculas e numeros (0-9)
    #
    #           Senha                |
    #
    #           Confirme a senha     | 
    #
    # >>>

        print(f"""
    *************************************************************\n
                    Cadastro de Novas Contas\n\n
             Cadastrar novo cliente:\n""")
        if vars > 0:
            print(f"                Nome:          {nome}")
        if vars > 1:
            print(f"                Idade:         {idade}\n")
        if vars > 2:
            print(f"""                Deposito inicial (min. R${min_dep})   {valor}\n
             Agora digite uma senha (minimo 6 e maximo 20 caracteres)
                - use MAIUSCULAS, minusculas e numeros (0-9)\n""")
        # if vars > 3:
        #     print(f"    #             Senha              {senha}\n")


    def cad_error(self, vars, num_error):
        err_message = ""
        os.system(osclear)
    # *************************************************************
    #
    #                 Cadastro de Novas Contas
    #    
    #
    #        Cadastrar novo cliente:
    #    
    #           Nome:          |
    #           Idade:         |
    #          
    #           Deposito inicial (min. R$10.00)   |
    #
    #        Agora digite uma senha (minimo 6 e maximo 20 caracteres)
    #           - use MAIUSCULAS, minusculas e numeros (0-9)
    #
    #           Senha                |
    #
    #           Confirme a senha     | 
    #
    # >>>
        if vars == 0:
            if num_error == 1:
                err_message = "Nome nao pode ficar vazio!"
            print(f"\n\n\n\n\n\n\n\n\n\n\n   <<< {err_message} >>>")
        elif vars == 1:
            if num_error == 1:
                err_message = "Idade nao pode ficar vazia!"
            elif num_error == 2:
                err_message = "Idade tem que ser numerica!"
            elif num_error == 3:
                err_message = "Valor invalido para Idade - tem que ser maior que 0!"
            elif num_error == 4:
                err_message = "Valor invalido para Idade - valor maximo aceito e 130!"
            print(f"\n\n\n\n\n\n\n\n\n\n   <<< {err_message} >>>")
        elif vars == 2:
            if num_error == 1:
                err_message = "Valor do deposito inicial nao pode ficar vazio!"
            elif num_error == 2:
                err_message = "Valor do deposito inicial tem que ser numerico!"
            elif num_error == 3:
                err_message = "Valor invalido para deposito inicial - tem que ser maior que R$10.00!"
            print(f"\n\n\n\n\n\n\n\n   <<< {err_message} >>>")
        elif vars == 3:
            if num_error == 1:
                err_message = "Senha nao pode ficar vazia!"
            elif num_error == 2:
                err_message = "Senha nao pode ser menor que 6 caracteres!"
            elif num_error == 3:
                err_message = "Senha nao pode ser maior que 20 caracteres!"
            elif num_error == 4:
                err_message = "Senha tem que ter letras MAIUSCULAS, minusculas e numeros (0-9)!"
            print(f"\n\n\n   <<< {err_message} >>>")
        elif vars == 4:
            if num_error == 1:
                err_message = "As senhas informadas nao sao iguais! Digite novamente!"
            print(f"\n   <<< {err_message} >>>")
        else:
            pass
            # print(f"\n\n\n\n\n\n\n\n\n   >>> ")
        sleep(3)


    def login_conta(self, vars, nome="", senha=""):
        os.system(osclear)
    # *************************************************************
    #
    #
    #                  Acesso a Conta Corrente
    #    
    #    
    #         Digite o numero da sua conta e sua senha:
    #
    #    
    #                   Conta        | 
    #
    #                   Senha        |
    #
    #
    #
    #
    #
    #  >>>
    #
          


class Conta:
    def __init__(self):
        self.nome = ""
        self.idade = 0
        self.saldo = 0.0
        self.senha = ""


    def nova_conta():
        cadtela = Tela()
        print(f"""
    *************************************************************\n
                    Cadastro de Novas Contas\n\n
             Cadastrar novo cliente:\n""")
        while True:        
            nome = input(f"                Nome:          ")
            c_nome = checa_nome(nome)
            if c_nome != 0:
                cadtela.cad_error(0,c_nome)
                cadtela.cadastra_conta(0)
                continue
            else:
                break
        while True:
            idade = input(f"                Idade:         ")
            c_idade = checa_idade(idade)
            if c_idade != 0:
                cadtela.cad_error(1,c_idade)
                cadtela.cadastra_conta(1)
                continue
            else:
                break
        while True:           
            valor = input(f"\n                Deposito inicial (min. R$10.00)   ")
            c_valor = checa_valor(valor)
            if c_valor != 0:
                cadtela.cad_error(2,c_valor)
                cadtela.cadastra_conta(2)
                continue
            else:
                break
        print("""
             Agora digite uma senha (minimo 6 e maximo 20 caracteres)
                - use MAIUSCULAS, minusculas e numeros (0-9)\n""")
        while True:       
            senha = input(f"                Senha                ")
            c_senha = checa_senha(senha)
            if c_senha != 0:
                cadtela.cad_error(3,c_senha)
                cadtela.cadastra_conta(3)
                continue
            else:   
                contrasenha = input(f"\n                Confirme a senha     ")
                c_csenha = checa_csenha(senha, contrasenha)
                if c_csenha != 0:
                    cadtela.cad_error(4,c_csenha)
                    cadtela.cadastra_conta(3)
                    continue
                else:
                    break
        resposta = dbgeneral.add_conta(nome, idade, valor, senha)
        if resposta[0] == 0:
            rsp_message = f"Cliente {nome} cadastrado, o numero da conta e {resposta[1]}."
            print(f"\n   >>> {rsp_message} >>>")
            sleep(3)



    def consulta_saldo(self, conta):
        saldo = dbgeneral.ver_saldo(conta)
        retorno = [0, saldo]


    def retirada(self, conta, valor):
        saldo = dbgeneral.ver_saldo(conta)
        if saldo < valor:
            retorno = [1, saldo]
            return retorno
        else:
            dbgeneral.saque(conta,valor)
            saldo = dbgeneral.ver_saldo(conta)
            retorno = [0, saldo]
            return retorno


    def deposito(self, conta, valor):
        dbgeneral.deposito(conta,valor)
        saldo = dbgeneral.ver_saldo(conta)
        retorno = [0, saldo]

    
    def validacao(self, conta, senha):
        dbgeneral.validar(conta, senha)


