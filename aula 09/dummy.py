# valor1 = "10"

# valor2 = "12.00"

# valor3 = "0.1"

# def isfloat(test_value):
#     try: 
#         float(test_value)
#     except ValueError: 
#         return False
#     return True

# if isfloat(valor1) == True:
#     print(f"valor1  {valor1}  eh float")
# else:
#     print(f"valor1  {valor1}  nao eh float")

# if isfloat(valor2) == True:
#     print(f"valor2  {valor2}  eh float")
# else:
#     print(f"valor2  {valor2}  nao eh float")

# if isfloat(valor3) == True:
#     print(f"valor3  {valor3}  eh float")
# else:
#     print(f"valor3  {valor3}  nao eh float")

# mystring1 = "Carrapeta01"
# mystring2 = "Carrapeta01"

# hashed1 = hash(mystring1)
# hashed2 = hash(mystring2)

# print(f"String '{mystring1}' hash '{hashed1}' ")
# print(f"String '{mystring2}' hash '{hashed2}' ")

# from time import sleep
from hashlib import sha3_512, sha3_384, shake_256
from getpass import getpass
import secrets

# 
nome = 'Carrapeta01'
# gfg = shake_256() 
# gfg.update(nome.encode('ascii')) 
# print(f"{gfg.hexdigest(255)}\n")

# print(shake_256(nome.encode('ascii')).hexdigest(255))
# print("")
# print(sha3_384(nome.encode('ascii')).hexdigest())
# print("")
# print(sha3_512(nome.encode('ascii')).hexdigest())

# senha = getpass(prompt="               Digite a senha:        ")
# print(f"\n\n    A senha e:    '{senha}' ")

# key = secrets.token_hex(24)
print(f" 16: {secrets.token_hex(16)}\n")
print(f" 24: {secrets.token_hex(24)}\n")
print(f" 32: {secrets.token_hex(32)}\n")
print(f" 64: {secrets.token_hex(64)}\n")
print(f"128: {secrets.token_hex(128)}\n")