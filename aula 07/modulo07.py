# Tudo o que nao estiver incluido dentro de uma classe ou funcao sera executado no momento
# que o modulo for importado

# um modo de evitar isso ao importar modulos e incluir essas instrucoes dentro do IF abaixo;
# nesse caso o comando so sera executado se a rotina chamada for invocada ao rodar o 
# modulo07.py diretamente

def soma(x,y):
    return x + y

# print(__name__)

if __name__ == "__main__":
# A variavel __name__ eh criada dinamicamente pelo python e contem o nome do arquivo principal 
# usado para invocar o modulo (ou recebe o valor __main__ se invocada do proprio modulo)

    print("Mensagem sendo impressa a partir do arquivo modulo07.py")

    nome = "Luiz Polastre"
