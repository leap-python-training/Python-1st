#
import modulo07

# Tudo o que nao estiver incluido dentro de uma classe ou funcao sera executado no momento
# que o modulo for importado

# print("Mensagem sendo impressa a partir do arquivo aula07.py")

# =========================================================================

# Como tratar erros:

# try:  # Tente executar esse codigo:
# #    nome = int("abc")
#     nome = int("123")

# except ValueError:  # Ao encontrar este tipo de excecao, execute essas instrucoes
#     print("O programa deu uma excecao de ValueError, entre com um valor apropriado")
#     nome = "abc"

# except TypeError:  # Ao encontrar este tipo de excecao, execute essas instrucoes
#     print("O programa deu uma excecao de TypeError, veja o tamanho dos seus dedos!")

# else:  # Se nao deu nenhum erro
#     print("Nao deu nenhum erro.")

# finally:  # Sempre executa esse codigo no final, independente se houve erro ou nao
#     print("Sempre executa no final, FINALLY.")


# ========================================================================
    
# Bancos de dados no python
# SQL -> Armazena informacoes em forma de tabela

import sqlite3

conexao = sqlite3.connect("aula 07/empresa.db")  # Cria uma conexao com o banco de dados

cursor = conexao.cursor()  # Utilizado para editar o banco

cria_tabela = """  # ao criar a tabela incluir o IF NOT EXISTS para evitar erro se ja existir
CREATE TABLE IF NOT EXISTS funcionarios (
id integer primary key autoincrement,
nome text,
idade integer
)"""

# Ou podemos tambem colocar o comando dentro do TRY, com o EXCEPT do SQLite (e a instrucao
# pass para ignorar o erro e continuar) 
try:
    cursor.execute(cria_tabela)
except sqlite3.OperationalError:
    pass

# Criar um comando para adicionar valores em uma tabela

adiciona_funcionario1 = """
INSERT INTO funcionarios (nome, idade)
VALUES ("Luiz", 46)
"""

adiciona_funcionario2 = """
INSERT INTO funcionarios (nome, idade)
VALUES ("Eduardo", 38)
"""

adiciona_funcionario3 = """
INSERT INTO funcionarios (nome, idade)
VALUES ("Alves", 25)
"""

# cursor.execute(adiciona_funcionario1)
# cursor.execute(adiciona_funcionario2)
# cursor.execute(adiciona_funcionario3)
# conexao.commit()


# ==========================================================

# Para verificar o conteudo do banco de dados:
checa_tabela1 = "SELECT * FROM funcionarios"

# Para checar o banco de dados por registros especificos:
checa_tabela2 = "SELECT idade FROM funcionarios"
checa_tabela3 = "SELECT * FROM funcionarios WHERE nome = 'Luiz'"

# cursor.execute(checa_tabela2)

# conteudo = cursor.fetchall()

# for linha in conteudo:
#     print(linha)


# ==========================================================

# Para remover entradas da tabela:
    
remove_funcionario = "DELETE FROM funcionarios WHERE nome = 'Alves'"
cursor.execute(remove_funcionario)
conexao.commit()


# cursor.execute(checa_tabela1)
# conteudo = cursor.fetchall()

# for linha in conteudo:
#     print(linha)


# ==========================================================

# Para atualizar uma entrada na tabela:

atualiza_tabela = 'UPDATE funcionarios SET idade = 35 WHERE id = 2'    

cursor.execute(atualiza_tabela)
conexao.commit()

cursor.execute(checa_tabela1)
conteudo = cursor.fetchall()

for linha in conteudo:
    print(linha)
