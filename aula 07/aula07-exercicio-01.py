# Crie um script para simular uma lanchonete.
# 
# - Essa lanchonete deverá ter 5 tipos de comida e 3 tipos de bebidas no cardápio.
# - Quando clientes chegarem a essa lanchonete, eles deverão fazer um pedido aleatório de 1 comida + 1 bebida.
# - A lanchonete deverá ter um numero limitado de porções dessas comidas/bebidas. Comida = 5, bebida = 7.
# - Quando o cliente fazer o pedido, ele deverá ser notificado se as opções escolhidas estão disponiveis,
# e quais seus preços.

# Criar um DB.
# Criar uma tabela pra comida e uma pra bebida.
# Preencher essas tabelas.

# Criar uma função pra simular um cliente comprando algo.

import os, sqlite3
from time import sleep

if os.name == "nt":
    osclear = "cls"
else:
    osclear = "clear"

conexao = sqlite3.connect("aula 07/lanchonete.db")  # Cria uma conexao com o banco de dados
cursor = conexao.cursor()  # Utilizado para editar o banco

def cria_tabelas():

    cria_tabela1 = """  
CREATE TABLE IF NOT EXISTS bebidas (
id integer primary key autoincrement,
nome text,
disponivel boolean,
quantidade integer,
preco real
)"""

    cria_tabela2 = """  
CREATE TABLE IF NOT EXISTS comidas (
id integer primary key autoincrement,
nome text,
disponivel boolean,
quantidade integer,
preco real
)"""

    try:
        cursor.execute(cria_tabela1)
    except sqlite3.OperationalError:
        pass

    try:
        cursor.execute(cria_tabela2)
    except sqlite3.OperationalError:
        pass


def add_bebida():
    os.system(osclear)
    print("*==========================================================*")
    nome = input("Digite o nome da nova bebida:  ")
    disp = input("A nova bebida esta disponivel imediatamente (S/N) ?  ")
    if disp.upper() == "S":
        disp = True
    else:
        disp = False
    quant = input("Informe a quantidade da nova bebida disponivel em estoque \n (Para bebidas preparadas, informe 0)   ")
    preco = input("Informe o preco de venda da nova bebida:  ")
    print("*==========================================================*")

    adiciona_bebida = f"INSERT INTO bebidas (nome, disponivel, quantidade, preco) VALUES ('{nome}', '{disp}', {quant}, {preco})"

    cursor.execute(adiciona_bebida)
    conexao.commit()    
    print(f"\n '{nome}' adicionado ao cardapio de bebidas!\n\n")
    sleep(2)

    
def add_comida():
    os.system(osclear)
    print("*==========================================================*")
    nome = input("Digite o nome da nova comida:  ")
    disp = input("A nova comida esta disponivel imediatamente (S/N) ?  ")
    if disp.upper() == "S":
        disp = True
    else:
        disp = False
    quant = input("Informe a quantidade da nova comida disponivel em estoque \n (Para comidas preparadas, informe 0)   ")
    preco = input("Informe o preco de venda da nova comida:  ")
    print("*==========================================================*")

    adiciona_comida = f"INSERT INTO comidas (nome, disponivel, quantidade, preco) VALUES ('{nome}', '{disp}', {quant}, {preco})"

    cursor.execute(adiciona_comida)
    conexao.commit()    
    print(f"\n '{nome}' adicionado ao cardapio de comidas!\n\n")
    sleep(2)


def mantem_estoque():
    while True:
        os.system(osclear)
        print("*************************************************************")
        opcao = input(" Selecione  comida, bebida ou finalizar (C/B/F):  ")
        if opcao.upper() == "C":
            tipo_item = "comida"
            tabela = "comidas"
        elif opcao.upper() == "B":
            tipo_item = "bebida"
            tabela = "bebidas"
        elif opcao.upper() == "F":
            break
        else:
            print(f"\n  << Opcao invalida - escolha C, B ou F! >> \n")
            sleep(3)
            continue


        print(f"\n Qual {tipo_item} atualizar ?  ")
        comida = input(" -  ")
        if comida.isnumeric() == True:
            if comida == 0:
                continue
            else:
                checa_comida = f"SELECT * FROM {tabela} WHERE id = {comida}"
                cursor.execute(checa_comida)

                conteudo = cursor.fetchone()

                if conteudo[2] == "True":
                    disp = "S"
                else:
                    disp = "N"
                print(f"""\n Essa e a informacao do estoque sobre {conteudo[1]}:
    - Nome:                {conteudo[1]}
    - Esta disponivel ?    {disp}
    - Qtde. em estoque:    {conteudo[3]}
    - Preco unidade:       {conteudo[4]}
""")
                atualiza = input(" Atualizar o estoque ? (S confirma, qualquer outro valor cancela)  ")
                if atualiza.upper() == "S":
                    print("\n Entre com os valores atualizados:  (ou deixe em branco para nao alterar) \n")
                    n_nome = input("    - Novo nome:           ")
                    c_disp = input("    - Disponivel: (S/N)    ")
                    if c_disp != "":
                        if c_disp == 0:
                            n_disp = "False"
                        elif c_disp.isnumeric() == True:
                            n_disp = "True"
                        elif c_disp.upper() == "S":
                            n_disp = "True"
                        else:
                            n_disp = "False"
                    while True:
                        n_estoque = input("    - Qtde. em estoque:    ")
                        if n_estoque != "" and (n_estoque.isnumeric() == False or int(n_estoque) < 0):
                            print(f"\n  << {n_estoque} nao e valido como o valor do estoque! >>\n")
                            sleep(3)
                            os.system(osclear)
                            print(f"""*************************************************************
 Selecione  comida, bebida ou finalizar (C/B/F):  {opcao}\n
 Qual {tipo_item} atualizar ?  
  -  {comida}\n
 Essa e a informacao do estoque sobre {conteudo[1]}:
    - Nome:                {conteudo[1]}
    - Esta disponivel ?    {disp}
    - Qtde. em estoque:    {conteudo[3]}
    - Preco unidade:       {conteudo[4]}\n 
 Atualizar o estoque ? (S confirma, qualquer outro valor cancela)  {atualiza}\n")
 Entre com os valores atualizados:  (ou deixe em branco para nao alterar) \n")
    - Novo nome:           {n_nome}
    - Disponivel: (S/N)    {c_disp} """)
                            continue
                        else:
                            if n_estoque != "":
                                n_estoque = int(n_estoque)
                            break
                    while True:
                        n_preco = input("    - Novo preco unidade:  ")
                        if n_preco != "" and (n_preco.isnumeric() == False or float(n_preco) < 0.0):
                            print(f"\n  << {n_preco} nao e valido como o valor do novo preco! >>\n")
                            sleep(3)
                            os.system(osclear)
                            print(f"""*************************************************************
 Selecione  comida, bebida ou finalizar (C/B/F):  {opcao}\n
 Qual {tipo_item} atualizar ?  
  -  {comida}\n
 Essa e a informacao do estoque sobre {conteudo[1]}:
    - Nome:                {conteudo[1]}
    - Esta disponivel ?    {disp}
    - Qtde. em estoque:    {conteudo[3]}
    - Preco unidade:       {conteudo[4]}\n 
 Atualizar o estoque ? (S confirma, qualquer outro valor cancela)  {atualiza}\n")
 Entre com os valores atualizados:  (ou deixe em branco para nao alterar) \n")
    - Novo nome:           {n_nome}
    - Disponivel: (S/N)    {c_disp} 
    - Qtde. em estoque:    {n_estoque}""")
                            continue
                        else:
                            if n_preco != "":
                                n_preco = float(n_preco)
                            break
                    confirma_atual = input(f"\n Confirma a atualizacao da {tipo_item} ? (S/N)  ")
                    if confirma_atual.upper() == "S":
                        if n_nome == "" and c_disp == "" and n_estoque == "" and n_preco == "":
                            print("\n  << Nada a atualizar! >>\n")
                            sleep(2)
                            continue
                        if n_nome != "":
                            atualiza_tabela = f'UPDATE {tabela} SET nome = "{n_nome}" WHERE id = {conteudo[0]}'    
                            cursor.execute(atualiza_tabela)
                            conexao.commit()
                        if c_disp != "":
                            atualiza_tabela = f'UPDATE {tabela} SET disponivel = "{n_disp}" WHERE id = {conteudo[0]}'    
                            cursor.execute(atualiza_tabela)
                            conexao.commit()
                        if n_estoque != "":
                            atualiza_tabela = f'UPDATE {tabela} SET quantidade = {n_estoque} WHERE id = {conteudo[0]}'    
                            cursor.execute(atualiza_tabela)
                            conexao.commit()
                        if n_preco != "":
                            atualiza_tabela = f'UPDATE {tabela} SET preco = {n_preco}" WHERE id = {conteudo[0]}'    
                            cursor.execute(atualiza_tabela)
                            conexao.commit()
                    else:
                        print(f"\n  << Atualizacao nao realizada >> \n")
                        sleep(2)
                        continue



def faz_pedido():
    pedido = {}
    itens = 0
    get_quant = True
    terminou_pedido = False
    while terminou_pedido != True:
        os.system(osclear)
        print("*************************************************************")
        opcao = input(" Deseja pedir comida, bebida ou finalizar pedido (C/B/F)?  ")
        if opcao.upper() == "C":
            tipo_item = "comida"
            print("\n Qual comida deseja pedir ?  ")
            comida = input(" -  ")
            if comida.isnumeric() == True:
                if comida == 0:
                    continue
                else:
                    checa_comida = f"SELECT * FROM comidas WHERE id = {comida}"
                    cursor.execute(checa_comida)

                    conteudo = cursor.fetchone()

                    if conteudo[2] == "True":
                        print(f"\n O preco de {conteudo[1]} e R${conteudo[4]}\n")
                        while get_quant == True:
                            quantst = input(f" Quantos {conteudo[1]}s voce quer ?  ")
                            try:
                                quantid = int(quantst)
                            except ValueError:
                                print(f"\n  << Erro ao digitar a quantidade:  '{quantst}' nao e valido! >>")
                                sleep(2)
                                os.system(osclear)
                                print(f"""*************************************************************
 Deseja pedir comida, bebida ou finalizar pedido (C/B/F)?  {opcao} \n
 Qual {tipo_item} deseja pedir ?  
 -  {comida} \n
 O preco de {conteudo[1]} e R${conteudo[4]}\n""")
                                continue
                            else:
                                break
                        if quantid < 1:
                            continue
                        if conteudo[3] == 0 or quantid < conteudo[3]:
                            itens += 1
                            detalhes = [conteudo[1], quantid, conteudo[4]]
                            pedido[itens] = detalhes
                            print(f"\n > {quantid} {conteudo[1]} adicionados a sua comanda! ")
                            sleep(2)
                            if conteudo[3] > 0:
                                nova_quant = conteudo[3] - quantid
                                atualiza_estoque = f'UPDATE comidas SET quantidade = {nova_quant} WHERE id = {conteudo[0]}'   
                                cursor.execute(atualiza_estoque)
                                conexao.commit()
                        else:
                            print(f"\n  << Nao e possivel pedir {quantid} de {conteudo[1]}, >>")
                            print(f"  << so ha {conteudo[3]} disponiveis no momento!      >>\n")
                            sleep(3)
                            continue
                    else:
                        print(f" << {conteudo[1]} nao esta disponivel! >>")
                        sleep(3)
                        continue
            else:
                comida = comida.lower()
                comida = comida.capitalize()
                checa_comida = f"SELECT * FROM comidas WHERE nome = '{comida}'"
                cursor.execute(checa_comida)

                conteudo = cursor.fetchone()
                if conteudo[2] == "True":
                    print(f" O preco de {conteudo[1]} e R${conteudo[4]}\n")
                    while get_quant == True:
                        quantst = input(f" Quantos {conteudo[1]}s voce quer ?  ")
                        try:
                            quantid = int(quantst)
                        except ValueError:
                            print(f"\n  << Erro ao digitar a quantidade:  '{quantst}' nao e valido! >>")
                            sleep(2)
                            os.system(osclear)
                            print(f"""*************************************************************
 Deseja pedir comida, bebida ou finalizar pedido (C/B/F)?  {opcao} \n
 Qual {tipo_item} deseja pedir ?  
 -  {comida}
 O preco de {conteudo[1]} e R${conteudo[4]}\n""")
                            continue
                        else:
                            break
                    if quantid < 1:
                        continue
                    if conteudo[3] == 0 or quantid < conteudo[3]:
                        itens += 1
                        detalhes = [conteudo[1], quantid, conteudo[4]]
                        pedido[itens] = detalhes
                        print(f"\n > {quantid} {conteudo[1]} adicionados a sua comanda! ")
                        sleep(2)
                    if conteudo[3] > 0:
                            nova_quant = conteudo[3] - quantid
                            atualiza_estoque = f'UPDATE comidas SET quantidade = {nova_quant} WHERE id = {conteudo[0]}'   
                            cursor.execute(atualiza_estoque)
                            conexao.commit()
                    else:
                        print(f"\n  << Nao e possivel pedir {quantid} de {conteudo[1]}, >>")
                        print(f"  << so ha {conteudo[3]} disponiveis no momento!      >>\n")
                        sleep(3)
                        continue
                else:
                    print(f" << {conteudo[1]} nao esta disponivel! >>")
                    sleep(3)
                    continue
        elif opcao.upper() == "B":
            tipo_item = "bebida"
            print("\nQual bebida deseja pedir ?  ")
            bebida = input(" -  ")
            if bebida.isnumeric() == True:
                if bebida == 0:
                    continue
                else:
                    checa_bebida = f"SELECT * FROM bebidas WHERE id = {bebida}"
                    cursor.execute(checa_bebida)
                    conteudo = cursor.fetchone()
                    if conteudo[2] == "True":
                        print(f" O preco de {conteudo[1]} e R${conteudo[4]}\n")
                        while get_quant == True:
                            quantst = input(f" Quantos {conteudo[1]}s voce quer ?  ")
                            try:
                                quantid = int(quantst)
                            except ValueError:
                                print(f"\n  << Erro ao digitar a quantidade:  '{quantst}' nao e valido! >>")
                                sleep(2)
                                os.system(osclear)
                                print(f"""*************************************************************
 Deseja pedir comida, bebida ou finalizar pedido (C/B/F)?  {opcao} \n
 Qual {tipo_item} deseja pedir ?  
 -  {bebida}
 O preco de {conteudo[1]} e R${conteudo[4]}\n""")
                                continue
                            else:
                                break
                        if quantid < 1:
                            continue
                        if conteudo[3] == 0 or quantid < conteudo[3]:
                            itens += 1
                            detalhes = [conteudo[1], quantid, conteudo[4]]
                            pedido[itens] = detalhes
                            print(f"\n > {quantid} {conteudo[1]} adicionados a sua comanda! ")
                            sleep(2)
                            if conteudo[3] > 0:
                                nova_quant = conteudo[3] - quantid
                                atualiza_estoque = f'UPDATE bebidas SET quantidade = {nova_quant} WHERE id = {conteudo[0]}'   
                                cursor.execute(atualiza_estoque)
                                conexao.commit()
                        else:
                            print(f"\n  << Nao e possivel pedir {quantid} de {conteudo[1]}, >>")
                            print(f"  << so ha {conteudo[3]} disponiveis no momento!      >>\n")
                            sleep(3)
                            continue
                    else:
                        print(f" << {conteudo[1]} nao esta disponivel! >>")
                        sleep(3)
                        continue
            else:
                bebida = bebida.lower()
                bebida = bebida.capitalize()
                checa_comida = f"SELECT * FROM bebidas WHERE nome = '{bebida}'"
                cursor.execute(checa_bebida)

                conteudo = cursor.fetchone()
                if conteudo[2] == "True":
                    print(f" O preco de {conteudo[1]} e R${conteudo[4]}\n")
                    while get_quant == True:
                        quantst = input(f" Quantos {conteudo[1]}s voce quer ?  ")
                        try:
                            quantid = int(quantst)
                        except ValueError:
                            print(f"\n  << Erro ao digitar a quantidade:  '{quantst}' nao e valido! >>")
                            sleep(2)
                            os.system(osclear)
                            print(f"""*************************************************************
 Deseja pedir comida, bebida ou finalizar pedido (C/B/F)?  {opcao} \n
 Qual {tipo_item} deseja pedir ?  
 -  {bebida}
 O preco de {conteudo[1]} e R${conteudo[4]}\n""")
                            continue
                        else:
                            break
                    if quantid < 1:
                        continue
                    if conteudo[3] == 0 or quantid < conteudo[3]:
                        itens += 1
                        detalhes = [conteudo[1], quantid, conteudo[4]]
                        pedido[itens] = detalhes
                        print(f"\n > {quantid} {conteudo[1]}s adicionados a sua comanda! ")
                        sleep(2)
                        if conteudo[3] > 0:
                            nova_quant = conteudo[3] - quantid
                            atualiza_estoque = f'UPDATE bebidas SET quantidade = {nova_quant} WHERE id = {conteudo[0]}'   
                            cursor.execute(atualiza_estoque)
                            conexao.commit()
                    else:
                        print(f"\n  << Nao e possivel pedir {quantid} de {conteudo[1]}, >>")
                        print(f"  << so ha {conteudo[3]} disponiveis no momento!      >>\n")
                        sleep(3)
                        continue
                else:
                    print(f" << {conteudo[1]} nao esta disponivel! >>")
                    sleep(3)
                    continue
        elif opcao.upper() == "F":
            os.system(osclear)
            valor_total = 0.0
            print("*************************************************************")
            print("  Pedido finalizado: \n")
            total_itens = itens + 1
            print("N.o   Item                 Quant  Preco unid.   Preco  ")
 #                   1   Salada                 1  - R$19.99       R$19.99
            for linha in range(1,total_itens):
                item_pedido = pedido[linha]
#                print(f"{item_pedido}")
                total_item = item_pedido[1] * (item_pedido[2] * 100)
                valor_total += total_item
                plinha = str(linha)
                pquant = str(item_pedido[1])
                ppreco = str(item_pedido[2])
                ptotal_item = str(total_item/100)
                print(f"{plinha.rjust(3,' ')}   {item_pedido[0].ljust(20,' ')}   {pquant.rjust(2,' ')}     R${ppreco.rjust(6,' ')}    R${ptotal_item.rjust(6,' ')}")
            pvalor_total = str(valor_total/100)
            print(f"""                                               ========== 
      --- Valor total do pedido ---            R${pvalor_total.rjust(7,' ')} \n""")
            break
        else: 
            print("\n  << Opcao incorreta - selecione C, B ou F >> \n")
            sleep(3)
    
               




                    


cria_tabelas()

add_comida()

add_comida()

add_comida()

add_comida()

add_comida()

add_bebida()

add_bebida()

add_bebida()

# mantem_estoque()

faz_pedido()







