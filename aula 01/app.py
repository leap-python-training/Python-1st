# PRINT is the command used to diplay messages on the screen

print("Ola, bem vindo ao curso de Python!")

# INPUT is tthe command used to receive informations from the user

input("Qual o seu nome? ")

# VARIABLES are words that store a value

nome = "Luiz E. Polastre"

print(nome)

print(f"Ola {nome}, bem-vindo!" )

idade = input("Qual a sua idade ? ")

print(f"Entao voce tem {idade} anos...")