import sqlite3
from hashlib import sha3_512


conexao = sqlite3.connect("data/bank.db")  # Cria uma conexao com o banco de dados
cursor = conexao.cursor()  # Utilizado para editar o banco


# initial_admin = False


def cria_tabelas():

    cria_tabela1 = """  
CREATE TABLE IF NOT EXISTS contas (
conta integer primary key autoincrement,
nome text,
idade integer,
saldo real
)"""

    cria_tabela2 = """  
CREATE TABLE IF NOT EXISTS usersec (
conta integer primary key autoincrement,
senha text,
shash text,
adminid text
)"""

    try:
        cursor.execute(cria_tabela1)
    except sqlite3.OperationalError:
        pass

    try:
        cursor.execute(cria_tabela2)
    except sqlite3.OperationalError:
        pass


def add_conta(nome, idade, deposito, senha, admin=""):
    s_hash = sha3_512(senha.encode('ascii')).hexdigest()
    adiciona_conta = f"INSERT INTO contas (nome, idade, saldo) VALUES ('{nome}', {idade}, {deposito})"
    cursor.execute(adiciona_conta)
    adiciona_sec = f"INSERT INTO usersec (senha, shash, adminid) VALUES ('{senha}', '{s_hash}', '{admin}')"
    cursor.execute(adiciona_sec)
    conexao.commit()
    pega_conta = f"SELECT conta FROM contas WHERE nome = '{nome}' and idade = {idade} and saldo = {deposito}"  
    cursor.execute(pega_conta)
    numero_conta = cursor.fetchone()
    resposta = [0,numero_conta]
    return resposta


def validar(conta, senha):
    test_hash = sha3_512(senha.encode('ascii')).hexdigest()
    seleciona_conta = f"SELECT shash, adminid FROM usersec WHERE conta = {conta}"
    cursor.execute(seleciona_conta)
    conteudo = cursor.fetchone()
    if conteudo[0] == test_hash:
        if conteudo[1] == "admin":
            resposta = [0,1]
        else:
            resposta = [0,0]
        return resposta
    else:
        resposta = [1,0]
        return resposta


def conta_existe(conta):
    seleciona_conta = f"SELECT nome FROM contas WHERE conta = {conta}"
    cursor.execute(seleciona_conta)
    conteudo = cursor.fetchone()
    if conteudo != "":
        resposta = 0
    else:
        resposta = 1
    return resposta


def ver_saldo(conta):
    seleciona_conta = f"SELECT nome, saldo FROM contas WHERE conta = {conta}"
    cursor.execute(seleciona_conta)
    conteudo = cursor.fetchone()
    resposta = [0, conteudo[0], conteudo[1]]
    return resposta


def deposito(conta, valor):
    seleciona_conta = f"SELECT saldo FROM contas WHERE conta = {conta}"
    cursor.execute(seleciona_conta)
    conteudo = cursor.fetchone()
    saldo = conteudo
    saldo += valor
    atualiza_tabela = f'UPDATE contas SET saldo = {saldo} WHERE conta = {conta}'    
    cursor.execute(atualiza_tabela)
    conexao.commit()
    seleciona_conta = f"SELECT saldo FROM contas WHERE conta = {conta}"
    cursor.execute(seleciona_conta)
    conteudo = cursor.fetchone()
    resposta = [0,conteudo]
    return resposta


def saque(conta, valor):
    seleciona_conta = f"SELECT saldo FROM contas WHERE conta = {conta}"
    cursor.execute(seleciona_conta)
    conteudo = cursor.fetchone()
    saldo = conteudo
    saldo -= valor
    atualiza_tabela = f'UPDATE contas SET saldo = {saldo} WHERE conta = {conta}'    
    cursor.execute(atualiza_tabela)
    conexao.commit()
    seleciona_conta = f"SELECT saldo FROM contas WHERE conta = {conta}"
    cursor.execute(seleciona_conta)
    conteudo = cursor.fetchone()
    resposta = [0,conteudo]
    return resposta


def checa_initial():
    global initial_admin
    checa_admin = f"SELECT adminid FROM usersec"
    cursor.execute(checa_admin)
    conteudo = cursor.fetchall()
    print(conteudo)
    for x in conteudo:
        print(x[0])
        if x == "admin":
            initial_admin = False
            break


def set_admin(conta):
    seleciona_conta = f"SELECT nome FROM contas WHERE conta = {conta}"
    cursor.execute(seleciona_conta)
    conteudo = cursor.fetchone()
    if conteudo == "":
        return 1
    else:
        atualiza_admin = f'UPDATE usersec SET adminid = "admin" WHERE conta = {conta}'    
        cursor.execute(atualiza_admin)
        conexao.commit()
        seleciona_conta = f"SELECT adminid FROM usersec WHERE conta = {conta}"
        cursor.execute(seleciona_conta)
        conteudo = cursor.fetchone()
        if conteudo == "admin":
            return 0
        else:
            return 2

