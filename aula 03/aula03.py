# Colecoes  -> Conjuntos de valores

# Strings, Tuplas, Listas, Dicionarios e Sets


# Tuplas ****************************************************************************

# Tuplas sao colecoes simples mas apos sua criacao nao podem mais serem alteradas, i.e.,
# Tuplas sao imutaveis
# Tuplas sao indexadas (comecando no 0), o index pode ser referenciado usando colchetes
variavel1 = ("Arroz", "Feijao", 50, 44.7, False)
# INDEX         0        1      2    3      4

# print(variavel1)
# print(variavel1[4])

# for x in variavel1:
#     print(f"Valor autual de x: {x}")


# Listas ***************************************************************************

# Listas sao similares as tuplas, mas as listas podem ser alteradas
# Listas sao indexadas (comecando no 0), o index pode ser referenciado usando colchetes
    
variavel2 = ["Arroz", "Feijao", 50, 44.7, False]
# INDEX         0        1      2    3      4

# variavel3 = variavel2.copy() # o metodo COPY clona a lista para a nova variavel

# print(variavel3[3])

# variavel3.append("Cachorro")  # o metodo APPEND pode acrescentar um elemento a lista

# print(variavel3.count("Arroz"))  # o metodo COUNT conta quantas vezes um elemento aparece na lista

# print(variavel3.index("Feijao")) # o metodo INDEX acha a 1.a ocorrencia de um elemento na lista

# print(len(variavel3))   # a funcao LEN retorna o numero de indices na lista

# variavel3.insert(2,True)  # o metodo INSERT acrescenta um elemento a lista em uma posicao especifica

# variavel3[2] = "Teste" # ao referenciar uma posicao especifica da lista pode alterar o conteudo

# variavel3.pop(0) # o metodo POP remove um elemento da lista informando o indice 

# variavel3.remove(44.7) # o metodo REMOVE remove a primeira ocorrencia de um elemento na lista identificado pelo valor

# print(variavel3)


# Dicionarios **********************************************************************

# Dicionarios sao definidos com o caracter de chaves
# Dados em dicionarios sao itens, definidos no formato -> Chave: Valor
# As chaves sao unicas, i.e. nao aceitam nomes repetidos

#             Chave: Valor    Chave: Valor     Chave: Valor
variavel4 = {"Nome": "Luiz", "Idade": 46,   "Altura": 1.75}
#                ITEM             ITEM         ITEM

#  Eh possivel definir listas como parte de itens

# lista = [1, 2, 3, ["Luiz", "Eduardo"]]
# variavel5 = {"Nome": "Luiz", "Idade": lista, "Altura": 1.75}

# print(variavel4["Idade"])

# for x in variavel4:  # assume a chave
#     print(x)

# for x in variavel4.keys(): # o metodo KEYS especifica a chave
#     print(x)

# for x in variavel4.values(): # o metodo VALUES especifica o valor
#     print(x)

# for x in variavel4.items(): # o metodo ITEMS especifica chave e valor
#     print(x)

# for x, y in variavel4.items(): # com duas variaveis, muda a apresentacao dos itens
#     print(x, y)

# variavel4["Peso"] = 110  # Adicionando um valor ao dicionario

# variavel4.pop("Idade")  # o metodo POP pode ser usado para eliminar o item especificado



# Sets  **************************************************************************

# Sets tambem sao definidos com o caracter de chaves
# Sets nao admitem valores repetidos

# variavel6 = {"Luiz", 45, "escopo", True, "Aula"}
# print(variavel6)

# lista = [10, 14, 18, 12, 18, 16, 14]

# print(lista)

# variavel7 = set(lista)
# lista=list(variavel7)

# print(lista)

