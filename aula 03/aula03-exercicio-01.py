import os

if os.name == "nt":
    osclear = "cls"
else:
    osclear = "clear"
# Exercicio 01 da aula 03:



saida1 = "N"
frutas={"bananas": 0, "melancias": 0, "morangos": 0}
total = 0.0
preco=[0.00, 0.75, 9.99, 0.20]

while saida1 == "N":
    os.system(osclear)
    print(""" 
=======================
Quitanda:
1: Ver cesta
2: Adicionar frutas
3: Sair
=======================
 """)
    opcao1 = input("Digite a opcao desejada: ")
    print()

    if opcao1 == "1":
        os.system(osclear)
        tot1 = frutas["bananas"] * preco[1]
        tot2 = frutas["melancias"] * preco[2]
        tot3 = frutas["morangos"] * preco[3]
        total = tot1 + tot2 + tot3
        print(f"""Conteudo da cesta:
   {frutas["bananas"]} bananas      R${tot1:.2f}
   {frutas["melancias"]} melancias    R${tot2:.2f}
   {frutas["morangos"]} morangos     R${tot3:.2f}

   Valor total da cesta:  R${total:.2f}
 """)
        input("Aperte qualquer tecla para continuar: ")

         
    elif opcao1 == "2":
        os.system(osclear)
        saida2 = "N"
        while saida2 =="N":

            print("""
===========================
Menu de frutas
 Digite a opcao desejada:
 Escolha a fruta desejada:
   1 - Banana       R$0.75
   2 - Melancia     R$9.99
   3 - Morango      R$0.20
===========================
 """)
            opcao2 = input("Digite a opcao desejada: ")

            if opcao2 == "1":
                opcao2 = int(opcao2)
                qtdeok = False
                while qtdeok == False:
                    qtde = input("Digite a quantidade desejada: ")
                    if qtde.isnumeric() == False:
                        print(f"{qtde} nao e valida! - digite a quantidade desejada")
                        print()
                        continue
                    else:
                        qtde = int(qtde)
                        qtdeok = True

                frutas["bananas"] = frutas["bananas"] + qtde
                print(f" {qtde} bananas adicionadas a cesta com sucesso!")
                print()
                input("Aperte qualquer tecla para continuar: ")
                saida2 = "S"

            elif opcao2 == "2":
                opcao2 = int(opcao2)
                qtdeok = False
                while qtdeok == False:
                    qtde = input("Digite a quantidade desejada: ")
                    if qtde.isnumeric() == False:
                        print(f"{qtde} nao e valida! - digite a quantidade desejada")
                        print()
                        continue
                    else:
                        qtde = int(qtde)
                        qtdeok = True

                
                frutas["melancias"] = frutas["melancias"] + qtde
                print(f" {qtde} melancias adicionadas a cesta com sucesso!")
                print()
                input("Aperte qualquer tecla para continuar: ")
                saida2 = "S"

            elif opcao2 == "3":
                opcao2 = int(opcao2)
                qtdeok = False
                while qtdeok == False:
                    qtde = input("Digite a quantidade desejada: ")
                    if qtde.isnumeric() == False:
                        print(f"{qtde} nao e valida! - digite a quantidade desejada")
                        print()
                        continue
                    else:
                        qtde = int(qtde)
                        qtdeok = True

                frutas["morangos"] = frutas["morangos"] + qtde
                print(f" {qtde} morangos adicionados a cesta com sucesso!")
                print()
                input("Aperte qualquer tecla para continuar: ")
                saida2 = "S"
                 
            else:
                print(f"Opcao {opcao2} invalida! - selecione 1, 2 ou 3")
                print()

    elif opcao1 == "3":
        
        os.system(osclear)
        saida1 = "S" 

    else:
        print(f"Opcao {opcao1} invalida! - selecione 1, 2 ou 3")
        print()      


