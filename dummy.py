import os

if os.name == "nt":
    osclear = "cls"
else:
    osclear = "clear"

os.system(osclear)

# valor1 = "10"

# valor2 = "12.00"

# valor3 = "0.1"

# def isfloat(test_value):
#     try: 
#         float(test_value)
#     except ValueError: 
#         return False
#     return True

# if isfloat(valor1) == True:
#     print(f"valor1  {valor1}  eh float")
# else:
#     print(f"valor1  {valor1}  nao eh float")

# if isfloat(valor2) == True:
#     print(f"valor2  {valor2}  eh float")
# else:
#     print(f"valor2  {valor2}  nao eh float")

# if isfloat(valor3) == True:
#     print(f"valor3  {valor3}  eh float")
# else:
#     print(f"valor3  {valor3}  nao eh float")

# mystring1 = "Carrapeta01"
# mystring2 = "Carrapeta01"

# hashed1 = hash(mystring1)
# hashed2 = hash(mystring2)

# print(f"String '{mystring1}' hash '{hashed1}' ")
# print(f"String '{mystring2}' hash '{hashed2}' ")

# from time import sleep
# from hashlib import sha3_512
# from getpass import getpass

# 
# nome = 'Carrapeta01'
# print(sha3_512(nome.encode('ascii')).hexdigest())

# senha = getpass(prompt="               Digite a senha:        ")
# print(f"\n\n    A senha e:    '{senha}' ")

# ==================================================================
# encriptacao com chaves simetricas:

from cryptography.fernet import Fernet

mensagem = "This is my test message!"

print(" ===== Teste de chave simetrica =====\n")

chave = Fernet.generate_key()
fernet = Fernet(chave)

mensagem_encr = fernet.encrypt(mensagem.encode())

print(f' - mensagem original:   "{mensagem}"\n')
print(f' - mensagem encriptada: "{mensagem_encr}"')

mensagem_decr = fernet.decrypt(mensagem_encr).decode()

print(f' - mensagem decriptada: "{mensagem_decr}"')

print(f'\n E essa e a chave usada:\n\n - chave : "{chave}"\n\n')

# ==================================================================
# encriptacao com chaves assimetricas:

import rsa

print(" === Teste de chaves assimetricas ===\n")

chavePublica, chavePrivada = rsa.newkeys(512)

# mensagem = "This is my test message!"

mensagem_encr = rsa.encrypt(mensagem.encode(),chavePublica)

print(f' - mensagem original:   "{mensagem}"')
print(f' - mensagem encriptada: "{mensagem_encr}"')

mensagem_decr = rsa.decrypt(mensagem_encr,chavePrivada).decode()

print(f' - mensagem decriptada: "{mensagem_decr}"')

print(f'\n E essas sao as chaves usadas:\n\n - chave publica: "{chavePublica}"')
print(f' - chave privada: "{chavePrivada}"\n')
